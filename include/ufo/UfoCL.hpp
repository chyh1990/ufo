#ifndef __UFOCL_HPP_
#define __UFOCL_HPP_

#include "llvm/Support/CommandLine.h"

/** Command line options for UFO */


namespace ufo
{ 
  enum Post { BOUND, BPRED, CPRED, BOX, BOXBPRED,  BOXCPRED, 
	      BOX_LIGHT_BPRED, BOX_LIGHT_CPRED,  BOXESBPRED, BOXESCPRED, 
	      BOXES, NONE }; 
  enum Refine { REF0, REF1, REF2, REF3, REF4, REF5 }; 
  enum Widen { WID0, WID1 }; 
  enum Cover { ADOM, IMP, GLOBAL };
  enum Validate { NEVER, LAST, ITER, POST };
  enum RefLabels { NODES, INV };
  enum Hints { HIN0, HIN1, HIN2 };
  enum BoundAlgo { BOU0, BOU1, BOU2 };
  enum ApronLib {ABOX, AOCT, APK };
}



namespace ufocl
{
  using namespace ufo;
  using namespace llvm;

  /** command line options */

  extern cl::opt<std::string> TRACE_FILENAME;

  extern cl::opt<unsigned> MIN_UNROLL ;

  extern cl::opt<unsigned> UNROLL_STEP ;

  extern cl::opt<bool> USE_INTS;
  
  extern cl::opt<enum Post> UFO_POST ;

  extern cl::opt<enum RefLabels> UFO_LBL;  
  
  extern cl::opt<bool> UFO_DEBUG;

  extern cl::opt<enum Validate> UFO_VALIDATE ;
  
  extern cl::opt<bool> UFO_TRACK_PTR ;

  extern cl::opt<bool> UFO_OD_ONLY ;

  extern cl::opt<unsigned> WIDEN_STEP ;

  extern cl::opt<enum Refine> INC_REFINE ;
  
  
  
  extern cl::opt<bool> CONS_REFINE ;
  

  extern cl::opt<bool> COMBINE;
  
  extern cl::opt<bool> UFO_CONJOIN_LABELS ;

  extern cl::opt<bool> UFO_CONJOIN_OLD_LABEL;
  

  extern cl::opt<bool> FALSE_EDGES;
  

  extern cl::opt<bool> UFO_DVO;
  
  extern cl::opt<bool> UFO_SIMPLIFY;
  extern cl::opt<enum Cover> COVER;
  extern cl::opt<enum Widen> WIDEN;

  extern cl::opt<enum Hints> HINTS;
  extern cl::opt<enum BoundAlgo> UFO_BOUND;
  extern cl::opt<unsigned> BOUND_WIDEN;

  extern cl::opt<enum ApronLib> APLIB;
}


#endif 
