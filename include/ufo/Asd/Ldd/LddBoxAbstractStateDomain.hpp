#ifndef __LDD_BOX_ABSTRACT_STATE_DOMAIN__HPP_
#define __LDD_BOX_ABSTRACT_STATE_DOMAIN__HPP_

#define ALPHA_GAMMA_DEBUG 1
//#define LDD_DEBUG_VERBOSE 1

#include "ufo/ufo.hpp"
#include "ufo/Asd/AbstractStateDomain.hpp"
#include "ufo/Asd/Interval.hpp"
#include "llvm/Support/InstIterator.h"

#include "exprVisit.hpp"

#include "LddToExpr.hpp"
#include "ufo/Asd/AbsAnalysis.hpp"

#include <stdio.h>

using namespace boost;

namespace ufo
{

  typedef std::pair<LddNodePtr,SEdge*> LddEdgePair;

  template<typename K, typename V>
  class LruCache
  {
  public:
    typedef LruCache<K,V> this_type;

  protected:
    typedef bimaps::bimap<bimaps::unordered_set_of<K>,
			  bimaps::list_of<V> > cache_type;
    typedef typename cache_type::left_value_type bimap_value_type;
    typedef typename cache_type::right_iterator right_iterator;

    cache_type cache;
    size_t capacity;
    
  public:
    typedef typename cache_type::left_const_iterator const_iterator;
    typedef typename cache_type::left_iterator iterator;

    LruCache (size_t c) : capacity (c) {}
    
    const_iterator find (const K &e) 
    {
      const_iterator it = cache.left.find (e);
      if (it != cache.left.end ())
	cache.right.relocate (cache.right.end (), 
			      cache.project_right (it));
      return it;
    }

    const_iterator end () const
    {
      return cache.left.end ();
    }
    
    std::pair<iterator,bool> insert (const K &e, V &v)
    {
      if (cache.size () == capacity)
	cache.right.erase (cache.right.begin ());

      return cache.left.insert (bimap_value_type (e, v));
    }

    size_t size () const { return cache.size (); }
  
    void clear () { cache.clear (); }
    
    
  };
  
    

  class LddAbstractStateValue : public AbstractStateValue
  {    
  protected:
    LddNodePtr val;
  public:
    LddAbstractStateValue (LddNodePtr n) : val(n) {}
    
    LddNodePtr getVal () const { return val; }

    void dump () const
    {
      LddManager *ldd = getLddManager (val);
      DdManager *cudd = Ldd_GetCudd (ldd);
      FILE *fp = Cudd_ReadStdout(cudd);
      Cudd_SetStdout(cudd,stderr);
      if (val.get () == Ldd_GetTrue (ldd)) errs() << "true\n";
      else if (val.get () == Ldd_GetFalse (ldd)) errs () << "false\n";	
      else Ldd_PrintMinterm(ldd,val.get ());
      Cudd_SetStdout(cudd,fp);      
    }
    
  };

  class LddBoxAbstractStateDomain : public AbstractStateDomain
  {
    friend class LDDToExpr::VarMapT<LddBoxAbstractStateDomain>;
    friend class SEdgeAbsPost<LddNodePtr,LddBoxAbstractStateDomain>;
  private:
    typedef std::map<const Value*, int> varmap_type;
    varmap_type varMap;
    std::map<int, const Value*> varMapRev;
    
  protected:
    Function &F;
    LBE &lbe;
    DominatorTree &DT;
    ExprFactory &efac;
    
    theory_t *theory;
    LddManager *ldd;

    ExprCache<LddNodePtr> alphaCache;
    LruCache<LddEdgePair,LddNodePtr> postCache;
    

    
    typedef std::map<CutPointPtr, std::vector<int> > LocIntVecMap;
    /** Maps locations to LDD variables that are out of scope at a
	location */
    LocIntVecMap outOfScopeVars;

  public:
    LddBoxAbstractStateDomain (Function &_F, LBE &_lbe, 
			       DominatorTree &dt,
			       ExprFactory &_efac) : 
      F(_F), lbe(_lbe), DT(dt), efac(_efac), alphaCache (50), postCache (50)
    {
      collectVars (F);
      buildOutOfScope ();

      DdManager* cudd = Cudd_Init (0, 0, CUDD_UNIQUE_SLOTS, 127, 0);
      if (ufocl::USE_INTS)
	theory = tvpi_create_boxz_theory (varMap.size ());
      else
	theory = tvpi_create_box_theory (varMap.size ());
      ldd = Ldd_Init (cudd, theory);
      
      //if(dvo) Cudd_AutodynEnable (cudd, CUDD_REORDER_GROUP_SIFT);
    }

    ~LddBoxAbstractStateDomain ()
    {
      postCache.clear ();
      alphaCache.clear ();

      DdManager *cudd = NULL;
      theory_t *theory = NULL;
      if (ldd) 
	{
	  cudd = Ldd_GetCudd (ldd);
	  theory = Ldd_GetTheory (ldd);
	  Ldd_Quit(ldd);
	}
      
      if (theory) tvpi_destroy_theory(theory);
      if (cudd) Cudd_Quit(cudd);
      
    }

    std::string name () { return "BOX"; };
    
    // -- don't use hints just yet
    virtual void refinementHint (CutPointPtr loc, Expr hint) {}
    virtual bool isFinite () { return false; }
    
    
    virtual AbstractState alpha (CutPointPtr loc, Expr val)
    {
      if (isOpX<TRUE> (val)) return top (loc);
      if (isOpX<FALSE> (val)) return bot (loc);
      
      Expr trueE = mk<TRUE> (efac);
      Expr falseE = mk<FALSE> (efac);

      {
	ExprCache<LddNodePtr>::const_iterator cEntry = alphaCache.find (val);
	if (cEntry != alphaCache.end ()) return absState (cEntry->second);
      }
      
      
      
      // -- approximate val by removing all non-box constraints
      cleanForBoxes cb (trueE, falseE);
      Expr approx1 = dagVisit (cb, val);

      // -- simplify
      Simp simp(trueE, falseE, efac);
      Expr approx2 = dagVisit (simp, approx1);

      if (isOpX<TRUE> (approx2)) return top (loc);
      if (isOpX<FALSE> (approx2)) return bot (loc);
      
      // -- finally convert into an LDD
      LddNodePtr res = approx (toBoxes (approx2));

      //ExprZ3 z3temp(efac);
      //std::cout << "SAT?: " << z3temp.isSAT(val);
      // -- don't expect result to be FALSE
      //assert (!isBot (res));
      
#ifdef ALPHA_GAMMA_DEBUG
      errs () << "ALPHA SIZE: " << Cudd_DagSize (&*res) 
	      << " from " << dagSize (val) << "\n";
#endif 
      alphaCache.insert (val, res);
      return absState (res);
    }
    
    virtual Expr gamma (CutPointPtr val, AbstractState v)
    {
      LddNodePtr n = getLddPtr (v);
      if (&*n == Ldd_GetTrue (ldd)) return mk<TRUE> (efac);
      if (&*n == Ldd_GetFalse (ldd)) return mk<FALSE> (efac);
      
      Expr res = gamma (n);

// #ifdef ALPHA_GAMMA_DEBUG
//       {
// 	AbstractState a = alpha (val, res);
// 	LddNodePtr x = getLddPtr (a);
// 	assert (x == n);
//       }
// #endif 

      

      return res;
    }
    
    
    virtual AbstractState top (CutPointPtr loc)
    {
      return absState (top ());
    }

    virtual bool isTop (CutPointPtr loc, AbstractState v) 
    {
      return isTop (getLddPtr (v));
    }
    

    virtual AbstractState bot (CutPointPtr loc)
    {
      return absState (bot ());
    }

    virtual bool isBot (CutPointPtr loc, AbstractState v) 
    {
      return isBot (getLddPtr (v));
    }
    
    virtual bool isLeq (CutPointPtr loc, AbstractState v1, AbstractState v2) 
    {
      LddNodePtr n1 = getLddPtr (v1);
      LddNodePtr n2 = getLddPtr (v2);
      return Ldd_TermLeq(ldd, &(*n1), &(*n2));
    }

    virtual bool isLeq (CutPointPtr loc, AbstractState v1, 
			AbstractStateVector const &u)
    {
      LddNodePtr c = bot ();
      forall (AbstractState s, u) 
	c = lddPtr (ldd, Ldd_Or (ldd, &*c, &*getLddPtr (s)));
    
      return Ldd_TermLeq (ldd, &*(getLddPtr (v1)), &*c);
    }
    
    
    virtual bool isEq (CutPointPtr loc, AbstractState v1, AbstractState v2) 
    {
      LddNodePtr n1 = getLddPtr (v1);
      LddNodePtr n2 = getLddPtr (v2);
      return n1 == n2;
    }

    virtual AbstractState join (CutPointPtr loc, 
				AbstractState v1, AbstractState v2)
    {
      LddNodePtr n1 = getLddPtr (v1);
      LddNodePtr n2 = getLddPtr (v2);
      return absState (join (getLddPtr (v1), getLddPtr (v2)));
    }

    virtual AbstractState meet (CutPointPtr loc, 
				AbstractState v1, AbstractState v2)
    {
      return absState (meet (getLddPtr (v1), getLddPtr (v2)));
    }

    virtual AbstractState widen (CutPointPtr loc, 
				 AbstractState v1, AbstractState v2)
    {
      LddNodePtr n1 = getLddPtr (v1);
      LddNodePtr n2 = getLddPtr (v2);
      return absState (lddPtr (ldd, Ldd_IntervalWiden (ldd, &*n1, &*n2)));
    }

    /** based on GCNR08. oldV is ordered by order of creation.  */
    virtual AbstractState widenWith (Loc loc, AbstractStateVector const &oldV, 
				     AbstractState newV)
    {
      LddNodePtr n = getLddPtr (newV);

      // -- v is an element of oldV
      LddNodePtr v;
      // -- u is join (v, n)
      LddNodePtr u;


      /** backwards, starting from the last produced value */
      rev_forall (AbstractState a, oldV)
	{
	  LddNodePtr x = getLddPtr (a);
	  // -- if v is defined, and current element is less than v, skip
	  if (v && Ldd_TermLeq (ldd, &*x, &*v)) continue;
	  
	  // -- at this point, a is NOT leq than v by assumption on
	  // -- the order of oldV, a is incomparable to v [* not sure
	  // -- this is true, but don't think it changes the widening
	  // -- *]

	  LddNodePtr w = join (x, n);
	  // -- if v is defined, w must be leq than u
	  if (!v || Ldd_TermLeq (ldd, &*w, &*u))
	    {
	      v = x;
	      u = w;
	    }
	  else /** out */ break;
	}
      return absState (lddPtr (ldd, Ldd_IntervalWiden (ldd, &*v, &*u)));
    }
    
    
    virtual AbstractState post (AbstractState pre, 
				CutPointPtr src, CutPointPtr dst)
    {
      LddNodePtr n = getLddPtr (pre);      
      return absState (post (n, src, dst));
    }
    
    virtual AbstractState post (const LocLabelStatePairVector &pre,
				Loc dst, 
				BoolVector &deadLocs)
    {
      LddNodePtr res = bot ();
      
      // -- store raw poiters in last since the reference to them is
      // -- kept in the pre vector
      std::map<Loc, LddNode*> last;

      size_t count = pre.size () - 1;
      rev_forall (LocLabelStatePair lv, pre)
	{
	  LddNodePtr val;
	  if (lv.second.second) val = getLddPtr (lv.second.second);
	  else val = getLddPtr (alpha (lv.first, lv.second.first));
	  
	  LddNode *lastV = last[lv.first];

	  // -- skip an element if a bigger element has been processed already
	  if (lastV != NULL && Ldd_TermLeq (ldd, &*val, lastV))
	    {
#ifdef ALPHA_GAMMA_DEBUG
	      errs () << "SKIPING POST AT COUNT " << count << "\n";
#endif
	      count--;
	      continue;
	    }
	  
	  
	  // -- do the actual post computation
	  LddNodePtr x = post (val, lv.first, dst);

#ifdef ALPHA_GAMMA_DEBUG
	  errs () << "POST " << count 
		  << " " << lv.first->getBB ()->getName () << " -> " 
		  << dst->getBB ()->getName () << "   "
		  << " in size " << Cudd_DagSize (&*val)
		  << " out size " << Cudd_DagSize (&*x) << "\n";
	  if (isTop (val)) errs () << "in is TOP";
	  if (isTop (x)) errs () << "out is TOP";
	  errs () << "\n";
#endif

	  // -- update result
	  if (isBot (x)) deadLocs [count] = true;
	  else res = join (res, x);
	  
	  // -- stop as soon as result is over-saturated
	  if (isTop (res)) break;
	  
	  // log what was processed
	  last [lv.first] = &*val;
	  count--;
	}

#ifdef ALPHA_GAMMA_DEBUG
      errs () << "Size after POST: " << Cudd_DagSize (&*res) 
	      << " on join set of " << pre.size () << "\n";
#endif
      return absState (res);
    }
    
    virtual AbstractState post_old (const LocStatePairVector &pre,
				    Loc dst, 
				    BoolVector &deadLocs)
    {
      LddNodePtr res = bot ();
      

      size_t count = 0;
      forall (LocStatePair lv, pre)
	{
	  LddNodePtr x = post (getLddPtr (lv.second), lv.first, dst);
#ifdef ALPHA_GAMMA_DEBUG
	  errs () << "POST " << count 
		  << " " << lv.first->getBB ()->getName () << " -> " 
		  << dst->getBB ()->getName () << "   "
		  << " in size " << Cudd_DagSize (&* (getLddPtr (lv.second)))
		  << " out size " << Cudd_DagSize (&*x) << "\n";
#endif
	  if (isBot (x)) deadLocs [count] = true;
	  else res = join (res, x);
	  // -- stop as soon as over-saturated
	  if (isTop (res)) break;
	  count++;
	}

#ifdef ALPHA_GAMMA_DEBUG
      errs () << "Size after POST: " << Cudd_DagSize (&*res) 
	      << " on join set of " << pre.size () << "\n";
#endif
      return absState (res);
    }
    

    

  protected:
    
    Expr gamma (LddNodePtr n)
    {
      LDDToExpr t = LDDToExpr (this);
      return cleanBox (t.toExpr (n, efac));
    }
    

    theory_t* getTheory() const { return Ldd_GetTheory (ldd); }
    LddManager* getManager() const { return ldd; }

    inline bool isTop (LddNodePtr v)
    {
      return &*v == Ldd_GetTrue (ldd);
    }
    inline LddNodePtr top () 
    {
      return lddPtr (ldd, Ldd_GetTrue (ldd));
    }
    inline LddNodePtr bot ()
    {
      return lddPtr (ldd, Ldd_GetFalse (ldd));
    }
    
    
    inline bool isBot (LddNodePtr v)
    {
      return &*v == Ldd_GetFalse (ldd);
    }
    
    
    
    inline LddNodePtr getLddPtr (AbstractState v)
    {
      return dynamic_cast<LddAbstractStateValue*>(&*v)->getVal ();
    }
    inline AbstractState absState (LddNodePtr n)
    {
      return AbstractState(new LddAbstractStateValue (n));
    }
    
    
    LddNodePtr post (LddNodePtr pre, CutPointPtr src, CutPointPtr dst)
    {
      edgeIterator it = src->findSEdge (*dst);
      assert (it != src->succEnd ());
      SEdgePtr edge = *it;

      // -- check cache
      LruCache<LddEdgePair,LddNodePtr>::const_iterator cEntry = 
      	postCache.find (LddEdgePair (pre, &*edge));
      if (cEntry != postCache.end ()) 
	return cEntry->second;      
      
      

      SEdgeAbsPost<LddNodePtr,LddBoxAbstractStateDomain> p(*this, pre, edge);
      LddNodePtr res = p.computePost ();
      
      
#ifdef LDD_DEBUG_VERBOSE
      LddNodePtr x(res);
#endif

      // -- get rid of out-of-scope variables
      std::vector<int> &out = outOfScopeVars [dst];
      if (out.size () == 1)
	res = lddPtr (ldd, Ldd_ExistsAbstract (ldd, &*res, out [0]));
      else if (out.size () > 1)
	res = lddPtr (ldd, 
		      Ldd_ExistAbstract (ldd, &*res, &out[0], out.size ()));
	  

#ifdef LDD_DEBUG_VERBOSE
      errs () << "POST over " << *(src->getBB ()) << " -> " 
	      << *(dst->getBB ()) << "\n"
	      << "of " << *(gamma (pre)) << "\n\n"
      	      << "is " << *(gamma (res)) << "\n\n"
	      << "was " << *(gamma (x)) << "\n\n";
      errs () << "ORIG: " << Cudd_DagSize (&*x) 
	      << ", NEW: " << Cudd_DagSize (&*res) << "\n";
      
#endif

      postCache.insert (LddEdgePair(pre, &*edge), res);
      
      return res;
    }
    
    

  protected:

    virtual LddNodePtr approx (LddNodePtr v) 
    {
      return lddPtr (ldd, Ldd_TermMinmaxApprox(ldd, &*v));
    }

    const Value* varToVal(int i) const
    {
      std::map<int, const Value*>::const_iterator it = varMapRev.find (i);
      assert (it != varMapRev.end ());
      return it->second;
    }


    
    void addVar(const Value* v)
    {
      assert(v);
      if (isVar(v)) return;
    
      int i = varMap.size();
      varMap[v] = i;
      varMapRev[i] = v;
    }

    bool isVar(const Value *v) const { return (getVarId(*v) >= 0); }

    const Value* varToVal(int i)
    {
      assert (varMapRev.count(i) > 0);
      return varMapRev[i];
    }

    int getVarId (const Value &v) const
    {
      varmap_type::const_iterator i = varMap.find (&v);
      return (i == varMap.end ()) ? -1 : i->second;
    }
    
    /** return term for variable v, neg for negation of variable */
    linterm_t termForVal(const Value* v, bool neg = false) const
    {
      int varId = getVarId (*v);
      assert (varId >=0 && "Variable not tracked");

      int sgn = neg ? -1 : 1;
      linterm_t term = theory->create_linterm_sparse_si (&varId, &sgn, 1);
      return term; 
    }


    /** Returns a pair (alpha(instr), alpha(!instr)) for a given
	compare instruction */
    LddNodePtrPair alphaCmp (const CmpInst *instr) 
    {
      //check number of operands -- must be 2
      if(instr->getNumOperands() != 2) return LddNodePtrPair(top(),top());


      //get the operands
      Value * op0 = instr->getOperand (0);
      Value * op1 = instr->getOperand (1);

      //get the predicate
      CmpInst::Predicate pred = instr->getPredicate();

      //if the first operand is a number swap operands
      if(isIntNumber(op0)) 
	{
	  Value *temp = op0;
	  op0 = op1;
	  op1 = temp;
	  pred = CmpInst::getSwappedPredicate(pred);
	}
      
      //both operands must not be numbers -- they would have been
      // folded
      if(isIntNumber(op0)) {
        errs() << "ERROR: illegal comparison between two numbers : " 
	       << *instr << '\n';
        assert(false);
      }

      //check if op0 is being tracked
      if(! isVar(op0)) return LddNodePtrPair(top(),top());

      return LddNodePtrPair(alphaHelper(pred,op0,op1),
			    alphaHelper(CmpInst::getInversePredicate(pred),
					op0,op1));
    }

    virtual LddNodePtr alphaHelper (CmpInst::Predicate pred,
				    const Value* op0, const Value* op1) 
    {
      LddNodePtr res = top ();
      if(!isIntNumber(op1)) return res;
      
      mpz_class op1Num = toMpz (op1);
      

      Expr v = mkTerm (op0, efac);
      Expr k = mkTerm (mpq_class (op1Num), efac);

      // -- create initial bounds
      bool isUns = CmpInst::isUnsigned(pred);

      switch(pred) 
	{
	case CmpInst::FCMP_FALSE:
	case CmpInst::FCMP_TRUE:
	  assert(false);
	  //equality
	case CmpInst::ICMP_EQ:
	  //if (! (isUns && op1Num == 0))
	  res = constrainWith (res, mk<GEQ> (v, k));
	  res = constrainWith (res, mk<LEQ> (v, k));	  
	  break;
	  //greater than
	case CmpInst::ICMP_UGT:
	case CmpInst::ICMP_SGT:
	  res = constrainWith (res, mk<GT> (v, k));
	  break;
	  //greater than or equal
	case CmpInst::ICMP_UGE:
	case CmpInst::ICMP_SGE:
	  res = constrainWith (res, mk<GEQ> (v, k));
	  break;
	  //less than
	case CmpInst::ICMP_ULT:
	case CmpInst::ICMP_SLT:
	  res = constrainWith (res, mk<LT> (v, k));
	  break;
	  //less than or equal
	case CmpInst::ICMP_ULE:
	case CmpInst::ICMP_SLE:
	  res = constrainWith (res, mk<LEQ> (v, k));
	  break;
	case CmpInst::ICMP_NE:        
	  // x is unsigned and we are comparing to 0
	  if (isUns && op1Num == 0) 
	    res = constrainWith (res, mk<GT> (v, k));
	  break;
	  //by default -- no information
	default:
	  break;
	}
      return res;
    }


    /** forget a variable from an abstract value */
    LddNodePtr forget (LddNodePtr n, const Value *v)
    {
      if (!isVar (v)) return n;
      if (isTop (n) || isBot (n)) return n;
      
      int id = getVarId (*v);
      return lddPtr (ldd, Ldd_ExistsAbstract (ldd, &*n, id));
    }

    /** alphaPost for v := k, where k is a constant */
    LddNodePtr alphaPost (LddNodePtr n, const Value *v, mpq_class k)
    {
      if (!isVar(v)) return n;
      if (isBot (n)) return n;
      

      linterm_t t = termForVal (v);
      constant_t kk = (constant_t) tvpi_create_cst (k.get_mpq_t ());
      LddNodePtr newVal = 
	lddPtr(ldd, Ldd_TermReplace(ldd, &(*n), t, NULL, NULL, kk, kk));
      theory->destroy_cst(kk);

      assert (!isBot (newVal));
      return newVal;
    }

    /** alphaPost for v := k, where k is an interval */
    LddNodePtr alphaPost (LddNodePtr n, const Value *v, 
			  const Interval &ival)
    {
      if(!isVar(v)) return n;
      if (isBot (n)) return n;

      constant_t kmin = NULL, kmax = NULL;

      if (!isNegInf (ival.first))
	{
	  mpq_class val = ival.first;
	  kmin = (constant_t)tvpi_create_cst (val.get_mpq_t ());
	}
      
      
      if (!isPosInf (ival.second))
	{
	  mpq_class val = ival.second;
	  kmax = (constant_t) tvpi_create_cst (val.get_mpq_t ());
	}
      
     

      linterm_t t = termForVal(v);
      LddNodePtr newVal = 
	lddPtr(ldd, Ldd_TermReplace(ldd, &(*n), t, NULL, NULL, kmin, kmax));

      if (kmin) theory->destroy_cst(kmin);
      if (kmax) theory->destroy_cst(kmax);
      
      assert (!isBot (newVal));
      return newVal;
    }

    /** v := u */
    LddNodePtr alphaPost (const LddNodePtr n, 
			  const Value *v,
			  const Value *u)
    {
      return alphaPost (n, v, u, 1, 0);      
    }
    
    /** alphaPost for v := a * u + k, where a, k are constants and u a
	variable */
    LddNodePtr alphaPost (const LddNodePtr n, 
			  const Value* v,
			  const Value* u,
			  mpq_class a, mpq_class k)
    {
      if (!isVar (v)) return n;
      if (!isVar (u)) return forget (n, v);
      if (isTop (n) || isBot (n)) return n;
      
      linterm_t t = termForVal (v);
      linterm_t r = termForVal (u);
      
      constant_t aa = (constant_t) tvpi_create_cst (a.get_mpq_t ());
      constant_t kk = (constant_t) tvpi_create_cst (k.get_mpq_t ());        
      LddNodePtr newVal = 
	lddPtr(ldd, Ldd_TermReplace(ldd,&(*n), t, r, aa, kk, kk));
      theory->destroy_cst(aa);
      theory->destroy_cst(kk);

      assert (!isBot (newVal));
      
      return newVal;
    }
    
    /** alphaPost for addition: v = a*u + b*w  */
    LddNodePtr alphaPost(const LddNodePtr n, const Value *v,
		     const Value *u, mpq_class a,
		     const Value *w, mpq_class b)  
    {
      return forget (n, v);
    }
    
    /** alphaPost for mult: v = u * w */
    LddNodePtr alphaPost (const LddNodePtr n, const Value *v,
			  const Value *u, const Value *w) 
    {
      return forget (n, v);
    }

    /** alphaPost for a function call */
    LddNodePtr alphaPost (const LddNodePtr n, const CallInst *ci) 
    { 
      return forget (n, ci);
    }
    
    
    
    /** construct outOfScopeVars */
    void buildOutOfScope ()
    {
      forall (CutPointPtr loc, lbe.getFunctionCutPts (F))
	buildOutOfScope (loc);

    }
    void buildOutOfScope (CutPointPtr loc)
    {
      // -- reference to the vector in the map
      std::vector<int> &out = outOfScopeVars [loc];

      const BasicBlock *bb = loc->getBB ();
      
      typedef std::pair<int, const Value*> MapEntry;
      forall (MapEntry kv, varMapRev)
	{
	  const Instruction *inst = dyn_cast<Instruction> (kv.second);
	  if (inst == NULL) errs () << "UNKNOWN " << *kv.second << "\n";
	  assert (inst != NULL);

	  // -- phi nodes of the current bb are in scope because we
	  // -- consider phi-nodes to belong to the predecessor edge
	  if (isa<PHINode> (inst) && inst->getParent () == bb) continue;
								 
	  // -- anything defined in the current bb does not dominate it
	  if (inst->getParent () == bb) out.push_back (kv.first);	  
	  
	  // -- inst does not dominate bb -> out of scope
	  else if (!DT.dominates (inst->getParent (), bb)) 
	    out.push_back (kv.first);

	  else
	    {
	      // -- keep any value that is defined and used in different
	      // -- blocks except if the only different-block-use of the
	      // -- value is in a PHI assignment of nodeBB. 
	      bool keep = false;
		  
	      for (Value::const_use_iterator ut = inst->use_begin(), 
		     ue = inst->use_end (); ut != ue; ++ut)
		{
		  const User* u = *ut;
		  const BasicBlock* ubb = 
		    cast<Instruction>(u)->getParent ();
		      
		  if (ubb != inst->getParent())
		    // -- found a use in a different block
		    // -- check if it is us
		    if (ubb == bb)
		      // -- use in the node BB. check if it is a phi use
		      if (isa<PHINode> (u)) continue;
		  
		  keep = true; 
		  break;
		}
	      if (!keep) out.push_back (kv.first);
	    } 
	}  
    }
    
    
    
    void collectVars(Function &F)
    {
      
      for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I)
	{
	  const Instruction *instr = &*I;

	  //compare
	  if(const CmpInst *ci = dyn_cast<CmpInst>(instr)) 
	    {
	      // -- track all compare instructions except for those
	      // -- that are only used in the terminator of the basic
	      // -- block they are defined in

	      // XXX This includes compare instructions that are only
	      // used in 'select' or 'or/and' statements inside BBs
	      // they are defined in. We can probably get rid of such
	      // compares as well.
	      const BasicBlock *ciBB = ci->getParent ();
	      const TerminatorInst *term = ciBB->getTerminator ();
	      
	      for (Value::const_use_iterator ut = ci->use_begin (),
		     ue = ci->use_end (); ut != ue; ++ut)
		{
		  const User *u = *ut;
		  const Instruction* uinst = cast<Instruction> (u);
		  // -- use outside of a terminator
		  if (uinst != term)
		    {
		      addVar (ci);
		      break;
		    }
		  // --  use in another BB
		  if (uinst->getParent () != ciBB) 
		    {
		      addVar (ci);
		      break;
		    }
		}
	      
	      //if the compare condition cannot be abstracted precisely,
	      //then we also track the compare instruction
	      //if(!isPreciselyAbs(ci->getPredicate())) addVar(instr);
	    }
	  // -- boolean binary ops
	  else if (isa<BinaryOperator> (*instr) && 
		   isBoolType (instr->getType ()))
	    {
	      const BasicBlock *pBB = instr->getParent ();
	      
	      for (Value::const_use_iterator ut = instr->use_begin (),
		     ue = instr->use_end (); ut != ue; ++ut)
		{
		  const User *u = *ut;
		  const Instruction* uinst = cast<Instruction> (u);
		  // -- use outside of a terminator
		  // --  use in another BB
		  if (uinst->getParent () != pBB) 
		    {
		      addVar (instr);
		      break;		    
		    }
		}
	    }
	  //PHI node
	  else if(isa<PHINode>(*instr)) addVar(instr);
	  //binary operation
	  else if(isa<BinaryOperator>(*instr))
	    { 
	      if (!(instr->getType ()->isIntegerTy (1))) addVar(instr);
	    }
	  //load
	  else if(isa<LoadInst>(*instr)) addVar(instr);
	  //return
	  else if(const ReturnInst *ri = dyn_cast<ReturnInst>(instr)) 
	    {
	      if(ri->getNumOperands() && !isIntNumber (ri->getOperand(0))) 
		addVar(ri->getOperand(0));
	    }
	  //call
	  else if (const CallInst *ci = dyn_cast<CallInst>(instr)) 
	    {
	      // XXX AG: We are tracking nondet functions?  XXX AG:
	      // XXX Think if this is necessary. There might be
	      // XXX conditionals based on them.
	      Function *fp = ci->getCalledFunction();
	      // -- track non-void functions, and functions for which there 
	      // -- is no llvm::Function object
	      if(!fp || 
		 (fp->getReturnType() != Type::getVoidTy(fp->getContext ())))
		addVar(instr);
	    }
	  //cast
	  else if(isa<CastInst>(*instr)) addVar(instr);
	  //select
	  else if(isa<SelectInst>(*instr)) { addVar(instr); }
	}
    }
    
    /** return true if comparison predicate can be precisely abstracted */
    virtual bool isPreciselyAbs(CmpInst::Predicate pred)
    {
      return (pred == CmpInst::ICMP_EQ || pred == CmpInst::ICMP_UGT ||
              pred == CmpInst::ICMP_SGT || pred == CmpInst::ICMP_UGE ||
              pred == CmpInst::ICMP_SGE || pred == CmpInst::ICMP_ULT ||
              pred == CmpInst::ICMP_SLT || pred == CmpInst::ICMP_ULE ||
              pred == CmpInst::ICMP_SLE);  
    }
    


    LddNodePtr toBoxes (Expr e)
    {
      //return LddNodePtr();
      
      std::map<Expr,LddNodePtr> cache;
      return toBoxes (e, cache);
    }
    LddNodePtr toBoxes (Expr e, std::map<Expr,LddNodePtr> &cache)
    {
      if (isOpX<TRUE>(e))
    	return lddPtr(ldd, Ldd_GetTrue(ldd)); 

      if (isOpX<FALSE>(e))
    	return lddPtr(ldd, Ldd_GetFalse(ldd));

      if (isOpX<NEG>(e))
    	{
    	  LddNodePtr arg = toBoxes (e->left (), cache);
    	  return lddPtr (ldd, Ldd_Not (&*arg));
    	}


      // -- lookup
      std::map<Expr,LddNodePtr>::const_iterator it = cache.find (e);
      if (it != cache.end ()) return it->second;

      if (isOpX<LEQ>(e) || isOpX<GEQ>(e) || isOpX<LT>(e)
    	  || isOpX<GT>(e) || isOpX<EQ>(e) || isOpX<NEQ>(e))
    	{
    	  LddNodePtr res = createTerm (e, ldd);
    	  cache[e] = res;
    	  return res;
    	}

      if (isOpX<VALUE>(e))
    	{
    	  // -- it must be a boolean variable
    	  // XXX assume that the lhs is always the value
    	  const Value* op0 = getTerm<const Value*> (e);
     
    	  assert (isBoolType(op0->getType()));

    	  LddNodePtr resB = assumeTrue (op0);
    	  return resB;
    	}

      // -- get all args
      std::vector<LddNodePtr> args;
      for (ENode::args_iterator it = e->args_begin(), 
    	     end = e->args_end(); it != end; ++it)
    	args.push_back (toBoxes (*it, cache));

      LddNodePtr res;

      if (isOpX<AND>(e))
    	{
    	  res = args[0];
    	  for (size_t i = 1; i < args.size(); ++i)
    	    res = lddPtr(ldd, Ldd_And(ldd, res.get (), args[i].get()));
    	}
    
      else if (isOpX<OR>(e))
    	{
    	  res = args[0];
    	  for (size_t i = 1; i < args.size(); ++i)
    	    res = lddPtr(ldd, Ldd_Or(ldd, res.get(), args[i].get()));
    	}

      else if (isOpX<XOR>(e))
    	{
    	  res = args[0];
    	  for (size_t i = 1; i < args.size(); ++i)
    	    res = lddPtr(ldd, Ldd_Xor(ldd, res.get(), args[i].get()));
	  
    	}
      else if (isOpX<IFF>(e))
    	{
    	  res = lddPtr(ldd, Ldd_Xor(ldd, args[0].get(), args[1].get()));
    	  res = lddPtr(ldd, Ldd_Not(res.get()));
    	}
    
      else if (isOpX<ITE>(e))
    	{
    	  res = lddPtr(ldd, 
    		       Ldd_Ite(ldd, args[0].get(), 
    			       args[1].get(), args[2].get()));
    	}

      assert (res);

      cache[e] = res;
      return res;
    }
    
    // -- ensures canonical form for terms: (var OP const)
    Expr flip(Expr e) const{
       assert(isOpX<LEQ>(e) || isOpX<GEQ>(e) || isOpX<LT>(e)
	      || isOpX<GT>(e) || isOpX<EQ>(e) || isOpX<NEQ>(e));
    
       if (isOpX<VALUE>(e->left())) return e;

       Expr lhs = e->right();
       Expr rhs = e->left();
       
       Expr res;
      if (isOpX<LEQ>(e))
	res = mk<GEQ>(lhs, rhs);
      else if (isOpX<GEQ>(e))
	res = mk<LEQ>(lhs, rhs);
      else if (isOpX<GT>(e))
	res = mk<LT>(lhs, rhs);
      else if (isOpX<LT>(e))
	res = mk<GT>(lhs, rhs);
      else if (isOpX<EQ>(e))
	res = mk<EQ>(lhs, rhs);
      else if (isOpX<NEQ>(e))
	res = mk<NEQ>(lhs, rhs);
      return res;
    }


    LddNodePtr createTerm (Expr e, LddManager* ldd)
    {
      e = flip(e);
      Expr lhs = e->left();
      Expr rhs = e->right();

      mpq_class lower =  negInf();
      mpq_class upper = posInf();
 
      // XXX assume that the lhs is always the value
      const Value* op0 = getTerm<const Value*> (lhs);
      assert (op0);
       
      mpq_class op1Num;
      

      if (isOpX<MPZ>(rhs))
	op1Num = getTerm<mpz_class> (rhs);
      else if (isOpX<MPQ>(rhs))
    	op1Num = getTerm<mpq_class> (rhs);
      else if (isOpX<INT>(rhs))
    	op1Num = getTerm<int>(rhs);
      // else if (isOpX<CONST_INT> (rhs))
      // 	op1Num = toMpz (getTerm<const ConstantInt*> (rhs)->getValue ());
      else
	assert (0 && "Unreachable");

      Expr k = mkTerm (op1Num, efac);

      LddNodePtr res = top ();
      if (isOpX<LEQ> (e) || isOpX<EQ> (e) || isOpX<NEQ> (e))
	res = constrainWith (res, mk<LEQ>(lhs, k));
      if (isOpX<GEQ> (e) || isOpX<EQ> (e) || isOpX<NEQ> (e))
	res = constrainWith (res, mk<GEQ>(lhs, k));

      if (isOpX<LT> (e))
	res = constrainWith (res, mk<LT>(lhs, k));
      else if (isOpX<GT> (e))
	res = constrainWith (res, mk<GT>(lhs, k));

      if (isOpX<NEQ> (e))
	res = lddPtr (ldd, Ldd_Not (&*res));

      return res;
    }


    LddNodePtr meet (LddNodePtr n1, LddNodePtr n2)
    {
      return lddPtr (ldd, Ldd_And (ldd, &*n1, &*n2));
    }
    LddNodePtr join (LddNodePtr n1, LddNodePtr n2)
    {
      return approx (lddPtr (ldd, Ldd_Or (ldd, &*n1, &*n2)));
    }
    
    LddNodePtr neq (LddNodePtr n1, LddNodePtr n2)
    {
      return lddPtr (ldd, Ldd_Xor (ldd, &*n1, &*n2));
    }
    LddNodePtr eq (LddNodePtr n1, LddNodePtr n2)
    {
      return lddPtr (ldd, Ldd_Not (Ldd_Xor (ldd, &*n1, &*n2)));
    }
    

    /** Generate an abstract value for var == true */
    LddNodePtr assumeTrue(const Value* var) 
    {
      if (!isVar (var)) return top ();
      
      return constrainWith (top (), 
			    mk<GEQ> (mkTerm (var, efac), 
				     mkTerm (mpq_class(1), efac)));
      // -- true is the interval from 1 to infinity
      // return assumeInterval (var, Interval(1, posInf ()));
    }
    
    /** Generate an abstract value for var == false, i.e., var != true */
    LddNodePtr assumeFalse(const Value* var) 
    {
      if (!isVar (var)) return top ();

      LddNodePtr res = assumeTrue (var);
      // -- if do not track var precisely, can't assume it is false
      return isTop (res) ? res : lddPtr (ldd, Ldd_Not (&*res));
    }

    /** 
	Restricts n by a given constraint e.

	Currently, e can be one of 
	TRUE | FALSE | x < k | x <= k | x > k | x >= k
	where x is a VALUE terminal and k an MPQ terminal
    */	
    LddNodePtr constrainWith (LddNodePtr n, Expr e)
    {
      if (isBot (n)) return n;
      if (e == mk<TRUE>(efac)) return n;
      if (e == mk<FALSE>(efac)) return bot ();
      
      assert (isOpX<LEQ> (e) || isOpX<LT> (e) || 
	      isOpX<GEQ>(e) || isOpX<GT> (e));

      assert (isOpX<VALUE> (e->left ()));
      assert (isOpX<MPQ> (e->right ()));

      const Value *var = getTerm<const Value*>(e->left ());
      if (!isVar (var)) return n;
      
      mpq_class bound = getTerm<mpq_class> (e->right ());


      /** upper bound */
      if (isOpX<LT> (e) || isOpX<LEQ> (e))
	{
	  constant_t cst = (constant_t) tvpi_create_cst (bound.get_mpq_t ());
	  linterm_t term = termForVal (var);
	  lincons_t cons = theory->create_cons (term, 
						isOpX<LT>(e) ? 1 : 0, cst);
	  LddNodePtr res = lddPtr (ldd, theory->to_ldd (ldd, cons));
	  theory->destroy_lincons (cons);
	  return lddPtr (ldd, Ldd_And (ldd, &*n, &*res));
	}

      /** lower bound */
      if (isOpX<GT> (e) || isOpX<GEQ> (e))
	{
	  // -- using transform
	  // x > k   <-> ! (x <= k) 
	  // x >= k  <-> ! (x < k)
	  constant_t cst = (constant_t) tvpi_create_cst (bound.get_mpq_t ());
	  linterm_t term = termForVal (var);
	  lincons_t cons = theory->create_cons (term, 
						isOpX<GEQ>(e) ? 1 : 0, cst);
	  LddNodePtr res = lddPtr (ldd, Ldd_Not (theory->to_ldd (ldd, cons)));
	  theory->destroy_lincons (cons);
	  return lddPtr (ldd, Ldd_And (ldd, &*n, &*res));
	}
      
      assert (0 && "UNREACHABLE");
      LddNodePtr res;
      return res;
    }    
  };

  class LddBoxesAbstractStateDomain : public LddBoxAbstractStateDomain
  {
  public:
    LddBoxesAbstractStateDomain (Function &_F, LBE &_lbe, 
				 DominatorTree &_dt,
				 ExprFactory &_efac, 
				 bool dvo = true) :
      LddBoxAbstractStateDomain (_F, _lbe, _dt, _efac) 
    {
      if(dvo) Cudd_AutodynEnable (Ldd_GetCudd (ldd), CUDD_REORDER_GROUP_SIFT);
    }    

    std::string name () { return "BOXES"; };    
    
  protected:
    virtual bool isPreciselyAbs (CmpInst::Predicate pred)
    {
      return pred == CmpInst::ICMP_NE || 
	LddBoxAbstractStateDomain::isPreciselyAbs (pred);
    }

    virtual LddNodePtr approx (LddNodePtr v) { return v; }
    
    virtual LddNodePtr alphaHelper (CmpInst::Predicate pred,
				    const Value* op0, const Value* op1) 
    {
      if (pred != CmpInst::ICMP_NE) 
	return LddBoxAbstractStateDomain::alphaHelper (pred, op0, op1);
      
      LddNodePtr res = top ();
      if(!isIntNumber(op1)) return res;
      mpz_class op1Num = toMpz (op1);
      

      Expr v = mkTerm (op0, efac);
      Expr k = mkTerm (mpq_class (op1Num), efac);

      // -- create initial bounds
      bool isUns = CmpInst::isUnsigned(pred);

      // only doing NE 
      // x is unsigned and we are comparing to 0
      if (isUns && op1Num == 0) 
	return constrainWith (res, mk<GT> (v, k));
      
      LddNodePtr t = constrainWith (top (), 
				    mk<LEQ> (v, k));
      t = constrainWith (t, mk<GEQ> (v, k));
      return lddPtr (ldd, Ldd_Or (ldd, &*res, Ldd_Not (&*t)));
    }
    
    virtual AbstractState widen (CutPointPtr loc, 
				 AbstractState v1, AbstractState v2)
    {
      LddNodePtr n1 = getLddPtr (v1);
      LddNodePtr n2 = getLddPtr (v2);
      return absState (lddPtr (ldd, Ldd_BoxWiden2 (ldd, &*n1, &*n2)));
    }

    virtual AbstractState widenWith (Loc loc, AbstractStateVector const &oldV, 
				     AbstractState newV)
    {
      LddNodePtr c = bot ();
      forall (AbstractState s, oldV) 
	c = lddPtr (ldd, Ldd_Or (ldd, &*c, &*getLddPtr (s)));
      
      LddNodePtr v = getLddPtr (newV);
      LddNodePtr cv = join (c, v);
      LddNodePtr w = lddPtr (ldd, Ldd_BoxWiden2 (ldd, &*c, &*cv));
      
      /** ensure that 'w' is only the fronteer of the compution. 
	  Not sure whether this is still a widening though 
      */
      w = lddPtr (ldd, Ldd_And (ldd, &*w, Ldd_Not (&*c)));
      /** ensure the output is at least as big as newV */
      w = lddPtr (ldd, Ldd_Or (ldd, &*w, &*v));
      
      return absState (w);
    }
    

  };
  
  /** Utility for debugging Ldd domains */
  class LddDebugAbstractDomain : public ProductAbstractStateDomain
  {
  public:
    
    LddDebugAbstractDomain (LddBoxesAbstractStateDomain &d1, 
			    LddBoxesAbstractStateDomain &d2) : 
      ProductAbstractStateDomain (d1, d2) {}

    AbstractState post (AbstractState pre, 
				CutPointPtr src, CutPointPtr dst)
    {
      return ProductAbstractStateDomain::post (pre, src, dst);
    }
    
    AbstractState post (const LocLabelStatePairVector &pre,
			Loc dst, 
			BoolVector &deadLocs)
    {
      AbstractState res = ProductAbstractStateDomain::post (pre, dst, deadLocs);
      AbsStatePair &v = getAbsVal (res);
      assert 
	(isSmtEq (lhsDom.gamma (dst, v.first), rhsDom.gamma (dst, v.second)));
      
      return res;
    }

    bool isSmtEq (Expr e1, Expr e2)
    {
      if (e1 == e2) return true;
      
      
      ExprZ3 z3 (e1->efac ());
      errs () << "e1 --> e2\n";      
      errs ().flush ();
      if (z3_is_sat (mk<AND> (e1, mk<NEG> (e2))) != false) return false;

      errs () << "e2 --> e1\n";
      errs ().flush ();
      if (z3_is_sat (mk<AND> (e2, mk<NEG> (e1))) != false) return false;
      
      return true;
    }
  };
    
}

  
  

#endif
