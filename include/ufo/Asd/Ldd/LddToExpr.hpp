#ifndef __LDD_TO_EXPR_H_
#define __LDD_TO_EXPR_H_

#include <boost/dynamic_bitset.hpp>
#include <boost/timer.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <iostream>
#include <map>

#include "ufo/Asd/Ldd/Ldd.hpp"

using namespace expr;
using namespace expr::op;

namespace ufo
{    

  class LDDToExpr
  {
  public:
    struct VarMap
    {
      virtual ~VarMap () {}
      virtual const Value *lookup (int i) const = 0;
    };
    
    template <typename T>
    class VarMapT : public VarMap
    {
      const T *vm;
      
    public:
      VarMapT (const T *m) : vm (m) {}
      const Value *lookup (int i) const { return vm->varToVal (i); }
    };

  protected:
    boost::shared_ptr<VarMap> varMap;
  public:

    template <typename T>
    LDDToExpr (const T *vm) : 
      varMap(new VarMapT<T>(vm)) {}

    Expr toExpr (LddNodePtr n, ExprFactory &efac)
    {
      LddManager *ldd = getLddManager (n);
      
      LddNode *N = Ldd_Regular(&(*n));
      if (Ldd_GetTrue (ldd) == N)
	return &*n == N ? mk<TRUE>(efac) : mk<FALSE>(efac);
      
      /** cache holds pointers because anything that is placed in the
	  cache will not disapear until 'n' is deleted */
      map<LddNode*, Expr> cache; Expr res = toExprRecur(ldd, &*n, efac, cache);
      
      return res;
    }
  protected: 
    Expr toExprRecur(LddManager* ldd, LddNode* n, 
		     ExprFactory &efac, map<LddNode*, Expr> &cache)
    {

      LddNode *N = Ldd_Regular (n);
      Expr res(NULL);

      if (N == Ldd_GetTrue (ldd)) res = mk<TRUE> (efac);
      else if (N->ref != 1)
	{
	  map<LddNode*,Expr>::const_iterator it = cache.find (N);
	  if (it != cache.end ()) res = it->second;
	}

      if (res) return N == n ? res : boolop::lneg (res);


      Expr c = exprFromCons (Ldd_GetCons(ldd, N), 
			     Ldd_GetTheory (ldd), efac);
      res = lite (c, 
		  toExprRecur (ldd, Ldd_T (N), efac, cache),
		  toExprRecur (ldd, Ldd_E (N), efac, cache));

      if (N->ref != 1) cache [N] = res;
      
      return n == N ? res : boolop::lneg (res);
    }

    Expr lite (Expr c, Expr t, Expr e)
    {
      if (t == e) return t;
      if (isOpX<TRUE> (c)) return t;
      if (isOpX<FALSE> (c)) return e;
      if (isOpX<TRUE> (t) && isOpX<FALSE> (e)) return c;
      if (isOpX<TRUE> (e) && isOpX<FALSE> (t)) return boolop::lneg (c);

      // XXX AG: Something does not like ITE expressions. Perhaps MathSat
      // iterprets them as non-logical ITEs
      //return mk<ITE> (c, t, e);
       return boolop::lor (boolop::land (c, t), 
			   boolop::land (mk<NEG>(c), e));
    }

    Expr exprFromCons(lincons_t lincons, theory_t *theory, ExprFactory &efac)
    {
      Expr lhs = exprFromTerm(theory->get_term(lincons), theory, efac);
      Expr rhs = exprFromCst(theory->get_constant(lincons), theory, efac);
      
      return theory->is_strict (lincons) ? mk<LT>(lhs, rhs) : mk<LEQ>(lhs, rhs);
    }

    Expr exprFromCst (constant_t cst, theory_t *theory, ExprFactory &efac)
    {
      mpq_class v;
      // XXX We know that the theory is tvpi, use its method direclty.
      tvpi_cst_set_mpq (v.get_mpq_t (), (tvpi_cst_t)cst);
      return mkTerm(v,efac);
    }

    Expr exprFromTerm (linterm_t term, theory_t *theory, ExprFactory &efac)
    {
      vector<Expr> coeffs (theory->term_size (term));

      for(size_t i = 0;i < coeffs.size (); i++)
	coeffs[i] = lmult (exprFromCst (theory->term_get_coeff (term,i), 
					theory, efac),
			   mkTerm(varMap->lookup 
				  (theory->term_get_var(term,i)), 
				  efac));

      return coeffs.size() > 1 ? 
	mknary<PLUS> (coeffs.begin (), coeffs.end ()) : coeffs [0];
    }

    Expr lmult(Expr cst, Expr var)
    {
      // Check if cst is equal to 1
      const MPQ& op = dynamic_cast<const MPQ&>(cst->op ());
      return op.get () == 1 ? var :  mk<MULT>(cst,var);
    }
 
  };

  
    
    
}

#endif
