#ifndef __EXPR_APRON__H_
#define __EXPR_APRON__H_

#include "ufoapron.hpp"

using namespace ufoapron;
using namespace expr;

namespace ufo
{
  namespace apron
  {
    struct TerminalAssert
    {
      template <typename emap_type, typename amap_type>
      static ap_state marshal (ap_manager_t* man, 
                               Expr e,
                               emap_type &eMap,
                               amap_type &aMap)
      {
        errs () << "UNKNOWN EXPRESSION: " << *e << "\n";
        assert (0 && "Unreachable");
        exit (1);
      }

      template <typename amap_type>    
      static Expr unmarshal (ap_manager_t* man,
                             ap_state s, 
                             ExprFactory &efac,
                             amap_type &aMap)
      {
        assert (0 && "Unreachable");
        exit (1);
      }
    };

    /**
     * Converter handling Numerical constraints
     */
    template <typename EC>
    struct NumericCons
    {
      template <typename emap_type, typename amap_type, typename var2dim_type>
      static ap_state marshal (ap_manager_t* man, 
                               Expr e,
                               emap_type &eMap,
                               amap_type &aMap,
                               var2dim_type &vMap)
      {
        size_t dimSize = vMap.size(); 
        if (isOp<ComparissonOp> (e))
        {
          ap_tcons0_array_t array = ap_tcons0_array_make (1);
          ap_tcons0_t cons = expr2ap_tcons (e, vMap);
          array.p[0] = cons; 

          ap_state res = ap_abstract0_of_tcons_array (man, 0, dimSize, &array);
          ap_tcons0_array_clear (&array);
            
          return res;
        }

        return EC::marshal (man, e, eMap, aMap);  
      }

      template <typename amap_type, typename dim2var_type>    
      static Expr unmarshal (ap_manager_t* man,
                             ap_state s, 
                             ExprFactory &efac,
                             amap_type &aMap,
                             dim2var_type &dMap)
      {
        return EC::unmarshal (man, s, efac, aMap); 
      }

      /**
       * Convert numeric expr to ap_texpr
       * param[in] Expr e
       * param[in] &vMap, map from variables to dimensions
       * return ap_texpr0_t*, apron tree expression
       */
      template <typename var2dim_type>
      static ap_texpr0_t* expr2ap_texpr (Expr e, var2dim_type &vMap)
      {
        int arity = e-> arity();

        if (arity == 0)
        {
          if (isOpX<VALUE> (e))
            return ap_texpr0_dim (vMap[e]);
          if (isOpX<MPQ> (e))
            return ap_texpr0_cst_scalar_mpq (getTerm<mpq_class>(e).get_mpq_t());

          assert (0 && "Unreachable");
        }
        else if (arity == 1)
        {
          if (isOpX<UN_MINUS>(e))
          {
            ap_texpr0_t* left = expr2ap_texpr (e->left(), vMap);
            return ap_texpr0_unop (AP_TEXPR_NEG, left, 
                                   AP_RTYPE_REAL, AP_RDIR_NEAREST);
          }

          assert (0 && "Unreachable");
        }
        else if (arity == 2)
        {
          //errs() << *e << "\n";
            
          if (isOpX<VARIANT> (e))
          {
            assert (vMap.count (e) > 0);
            return ap_texpr0_dim (vMap[e]);
          }

          ap_texpr0_t* left = expr2ap_texpr (e->left(), vMap);
          ap_texpr0_t* right = expr2ap_texpr (e->right(), vMap);

          if (isOpX<PLUS>(e))
            return ap_texpr0_binop (AP_TEXPR_ADD, left, right, 
                                    AP_RTYPE_REAL, AP_RDIR_NEAREST);
          if (isOpX<MINUS>(e))
            return ap_texpr0_binop (AP_TEXPR_SUB, left, right,
                                    AP_RTYPE_REAL, AP_RDIR_NEAREST);
          if (isOpX<MULT>(e))
            return ap_texpr0_binop (AP_TEXPR_MUL, left, right,
                                    AP_RTYPE_REAL, AP_RDIR_NEAREST);
          if (isOpX<DIV> (e))
            return ap_texpr0_binop (AP_TEXPR_DIV, left, right,
                                    AP_RTYPE_REAL, AP_RDIR_NEAREST);

          assert (0 && "Unreachable");
        }
        else
        {
          // n-nary
          //errs () << "N-ary: " << *e << "\n";
          assert (!isOpX<ITE> (e));

          vector<ap_texpr0_t*> args;
          assert (isOp<NumericOp> (e));

          for (ENode::args_iterator it = e->args_begin(), end = e->args_end();
               it != end; ++it)
            args.push_back (expr2ap_texpr(*it, vMap));

          if (isOpX<PLUS> (e))
            return ap_texpr0_naryop (AP_TEXPR_ADD, args, AP_RTYPE_REAL,
                                     AP_RDIR_NEAREST);
          if (isOpX<MINUS> (e))
            return ap_texpr0_naryop (AP_TEXPR_SUB, args, AP_RTYPE_REAL,
                                     AP_RDIR_NEAREST);
          if (isOpX<MULT> (e))
            return ap_texpr0_naryop (AP_TEXPR_MUL, args, AP_RTYPE_REAL,
                                     AP_RDIR_NEAREST);
          if (isOpX<DIV> (e))
            return ap_texpr0_naryop (AP_TEXPR_DIV, args, AP_RTYPE_REAL,
                                     AP_RDIR_NEAREST);

          assert (0 && "Unreachable");
          return NULL;
        }
      }

      /**
       * expr2ap_tcons.
       * Convert an Expr of linear constraint to an apron tree constraint object
       * param[in] Expr e
       * param[in] &vMap, map from variables to dimensions
       * return ap_tcons0_t, apron tree constraints
       */
      template <typename var2dim_type>
      static ap_tcons0_t expr2ap_tcons (Expr e, var2dim_type &vMap)
      {        
        assert (isOp<ComparissonOp>(e));

        if (isOpX<GEQ> (e))
        {
          return ap_tcons0_make
            (AP_CONS_SUPEQ, expr2ap_texpr (canonizeCons(e, true), vMap), NULL); 
        }
        else if (isOpX<GT> (e))
        {
          return ap_tcons0_make
            (AP_CONS_SUP, expr2ap_texpr (canonizeCons(e, true), vMap), NULL);
        }
        else if (isOpX<LT> (e))
        {
          return ap_tcons0_make
            (AP_CONS_SUP, expr2ap_texpr (canonizeCons(e, false), vMap), NULL);
        }
        else if (isOpX<LEQ> (e))
        {
          return ap_tcons0_make
            (AP_CONS_SUPEQ, expr2ap_texpr (canonizeCons(e, false), vMap), NULL);
        }
        else if (isOpX<EQ> (e))
        {
          return ap_tcons0_make 
            (AP_CONS_EQ, expr2ap_texpr (canonizeCons(e, true), vMap), NULL);
        }
        else if (isOpX<NEQ> (e))
        {
          return ap_tcons0_make
            (AP_CONS_DISEQ, expr2ap_texpr (canonizeCons(e, true), vMap), NULL);
        }
        else assert (0 && "Unreachable");

        return ap_tcons0_t();
      }

      /**
       * ap_lincons2expr.
       * param[in] ap_lincons0_t cons: apron linear constraint type
       * param[in] dim2var_type &dMap: map from dimension to variable
       * param[in] ExprFactory &efac
       * return Expr: expr representaion of the linear constraint
       */
      template <typename dim2var_type>
      static Expr ap_lincons2expr (ap_lincons0_t cons, dim2var_type &dMap, 
                                   ExprFactory &efac)
      {
        assert (cons.scalar == NULL); // Not modulo form

        ap_linexpr0_t* linexp = cons.linexpr0;
        assert (ap_linexpr0_is_linear (linexp));

        ExprVector _g;
        unsigned i;
        for (i=0; i<dMap.size(); ++i)
        {
          ap_coeff_t* coeff = ap_linexpr0_coeffref (linexp, i);
          if (ap_coeff_zero (coeff))
            continue;
            
          // COEFF * VAR
          _g.push_back (mk<MULT>(coeff2expr(coeff, efac), dMap[i]));
        }

        // Add possible constant
        ap_coeff_t* cst = ap_linexpr0_cstref (linexp);
        if (!ap_coeff_zero (cst))
          _g.push_back (coeff2expr(cst, efac));

        Expr zero = mkTerm (mpq_class(0), efac);
        Expr expr_line = mknary<PLUS> (zero,
                                       _g.begin(), _g.end());

        // Add comparisson operator
        Expr res;
        switch (cons.constyp)
        {
          case AP_CONS_EQ:
            res = mk<EQ> (expr_line, zero);
            break; 
          case AP_CONS_SUPEQ:
            res = mk<GEQ> (expr_line, zero);
            break;
          case AP_CONS_SUP:
            res = mk<GT> (expr_line, zero);
            break;
          case AP_CONS_EQMOD:
            assert (0 && "Unimplemented");
            break;
          case AP_CONS_DISEQ:
            res = mk<NEQ> (expr_line, zero);
        }
        assert (res);

        return res;
      }

      /**
       * coeff2expr.
       * param[in] ap_coeff_t* coeff: apron coefficient type
       * param[in] ExprFactory &efac: Expr Factory reference
       * return Expr<MPQ>: an expr representation of the coefficient
       */
      static Expr coeff2expr (ap_coeff_t* coeff, ExprFactory &efac)
      {
        assert (coeff->discr == AP_COEFF_SCALAR);
        ap_scalar_t* scalar = coeff->val.scalar;
        assert (scalar->discr == AP_SCALAR_MPQ);
        mpq_ptr c = scalar->val.mpq;

        return mkTerm (mpq_class(c), efac);
      }

      /**
       * canonize linear constraint, move terms on one side all 
       * to the other side.
       * param[in] Expr e, Expr representation of linear constraint
       * param[in] bool toLeft, move rigth to left / move right to left
       * return Expr, canonized expr
       */
      static Expr canonizeCons (Expr e, bool toLeft)
      {
        assert (isOp<ComparissonOp> (e));
        if (toLeft)
          return mk<PLUS> (e->left(), mk<UN_MINUS> (e->right()));
        else
          return mk<PLUS> (e->right(), mk<UN_MINUS> (e->left()));
      }

      /**
       * Supplement function that constructs n-ary texpr
       * param[in] ap_texpr_op_t op, operator type
       * param[in] vector<ap_texpr0_t*> &oprs, vector of operands
       * return ap_texpr0_t*, apron tree expression
       */
      static ap_texpr0_t* ap_texpr0_naryop (ap_texpr_op_t op,
                                            vector<ap_texpr0_t*> &oprs,
                                            ap_texpr_rtype_t type,
                                            ap_texpr_rdir_t dir)
      {
        int size = oprs.size();
        if (size <= 0) return NULL;
        else if (size == 1) return oprs[0];
        else
        {
          ap_texpr0_t* res = oprs[0];
          int i;
          for (i=1; i<size; ++i)
          {
            res = ap_texpr0_binop (op, res, oprs[i], type, dir);
          }
          return res;
        }
      }
    };

    /** 
     * Basic Boolean expressions
     * Convert Expr to aproximated Apron abstract state with
     * linear constraints.
     */
    template <typename EC> 
    struct BasicExprConverter
    {
      /**
       * This simplifier flatten the formula.
       * Change IFF, IMPL, XOR, ITE to AND, OR
       */
      struct DeepSimplifier
      {
        ExprFactory &efac;
        DeepSimplifier (ExprFactory &_e) : efac(_e) {}

        VisitAction operator () (Expr exp)
        {
          // (a && b) || (!a && !b)
          if (isOpX<IFF> (exp))
            return VisitAction::changeDoKids (
              boolop::lor (mk<AND> (exp->left(), exp->right()),
                           boolop::land
                           (boolop::lneg(exp->left()), boolop::lneg(exp->right()))));

          // !a || b
          if (isOpX<IMPL> (exp))
            return VisitAction::changeDoKids (
              boolop::lor (boolop::lneg (exp->left()), exp->right()));

          // a&&!b || !a&&b
          if (isOpX<XOR> (exp))
            return VisitAction::changeDoKids (
              boolop::lor (boolop::land (exp->left(), boolop::lneg(exp->right())),
                           boolop::land (boolop::lneg(exp->left()), exp->right())));
              
          if (isOp<ComparissonOp> (exp))
          {
            // x = ITE(b, y, z)
            // b/\(x=y) || !b/\(x=z)
            if (isOpX<EQ>(exp) && isOpX<ITE> (exp->right()))
            {
              Expr x = exp->left();
              Expr b = exp->right()->left();
              Expr y = exp->right()->right();
              Expr z = *((exp->right()->args_begin())+2);
              
              return VisitAction::changeTo (
                boolop::lor(
                  boolop::land (b, mk<EQ>(x,y)),
                  boolop::land (boolop::lneg(b), mk<EQ>(x,z))
                  )
                );
            }
          }

          return VisitAction::doKids();
        }
      };
      
      template <typename emap_type, typename amap_type, typename var2dim_type >
      static ap_state marshal (ap_manager_t* man, 
                               Expr e,
                               emap_type &eMap,
                               amap_type &aMap,
                               var2dim_type &vMap)
      {
        size_t dimSize = vMap.size();

        // Simplify the formula
        e = boolop::nnf (e);
        DeepSimplifier ds (e->efac());
        e = dagVisit (ds, e);
        e = boolop::simplify (e);

        if (isOpX<TRUE> (e)) return ap_abstract0_top (man, 0, dimSize);
        if (isOpX<FALSE> (e)) return ap_abstract0_bottom (man, 0, dimSize);
        
        int arity = e-> arity();
        
        // other terminal
        if (arity == 0 && isOpX<VALUE>(e))
          return ap_abstract0_top (man, 0, dimSize); 

        // this converter works only on Boolean expressions 
        else if (arity == 1)
        {
          if (isOpX<VARIANT>(e))
            return ap_abstract0_top (man, 0, dimSize);
          if (isOpX<NEQ> (e))
          {
            Expr _e = e->left();
            if (isOp<ComparissonOp>(_e))
            {
              Expr res;
              Expr left = _e->left();
              Expr right = _e->right();
              if (isOpX<LEQ>(_e))
                res = mk<GT>(left, right);
              if (isOpX<GEQ>(_e))
                res = mk<LT>(left, right);
              if (isOpX<GT>(_e))
                res = mk<LEQ>(left, right);
              if (isOpX<LT>(_e))
                res = mk<GEQ>(left, right);
              if (isOpX<EQ>(_e))
                res = mk<NEQ>(left, right);
              if (isOpX<NEQ>(_e))
                res = mk<EQ>(left, right);

              //errs () << "e: " << *e << "\n";
              assert (res);
              //errs() << "Res: " << *res << "\n";
              return EC::marshal(man, res, eMap, aMap, vMap);
            }
            else if (isOpX<VARIANT> (_e) || isOpX<BIND> (_e))
              return ap_abstract0_top (man, 0, dimSize);
          }
            
          assert (0 && "unreachable");
        }
        else if (arity == 2)
        {
          // XXX tuple need to be handled!
          if (isOpX<VARIANT>(e) || isOpX<VALUE>(e) || isOpX<BIND>(e))
            return ap_abstract0_top (man, 0, dimSize);
              
          if (isOp<ComparissonOp>(e))
            return EC::marshal (man, e, eMap, aMap, vMap);

          ap_state left = marshal (man, e->left(), eMap, aMap, vMap);
          ap_state right = marshal (man, e->right(), eMap, aMap, vMap);

          if (isOpX<AND> (e))
            return ap_abstract0_meet (man, true, left, right);      
          else if (isOpX<OR> (e))
            return ap_abstract0_join (man, true, left, right);
        }
        else if (arity > 2)
        {
          vector<ap_state> states;
            
          for (ENode::args_iterator it = e->args_begin (), 
                 end = e->args_end (); it != end; ++it)
          {
            states.push_back (marshal (man, *it, eMap, aMap, vMap));
          }

          if (isOpX<AND> (e))
          {
            return ap_abstract0_meet_array (man, &states[0], states.size());
          }
          else if (isOpX<OR> (e))
          {
            return ap_abstract0_join_array (man, &states[0], states.size());
          }
          else assert (0 && "unreachable");
        }
       
        return EC::marshal (man, e, eMap, aMap, vMap);  
      }

      /**
       * Convert vector of constraints to corresponding apron state.
       */
      template <typename var2dim_type>
      static ap_state marshal_cons (ap_manager_t* man,
                                    ExprVector &cons,
                                    var2dim_type &vMap)
      {
        size_t size = cons.size();
        ap_tcons0_array_t array = ap_tcons0_array_make (size);

        unsigned i;
        for (i=0; i<size; ++i)
        {
          ap_tcons0_t tcons = EC::expr2ap_tcons (cons[i], vMap);            
          array.p[i] = tcons; 
        }
          
        ap_state res  = ap_abstract0_of_tcons_array (man, 0, vMap.size(), &array);
        ap_tcons0_array_clear (&array);

        return res;
      }
      
      /**
       * unmarshal
       * Precise conversion from ap_state (lincons array) to Expr.
       */
      template <typename amap_type, typename dim2var_type>    
      static Expr unmarshal (ap_manager_t* man,
                             ap_state s, 
                             ExprFactory &efac,
                             amap_type &aMap,
                             dim2var_type &dMap)
      { 
        ap_lincons0_array_t consArray = 
          ap_abstract0_to_lincons_array (man, s);

        unsigned i;
        ExprVector _g;
        for (i=0; i< consArray.size; ++i)
        {
          _g.push_back (EC::ap_lincons2expr (consArray.p[i], dMap, efac));
        }
        return mknary<AND>(mk<TRUE>(efac), _g.begin(), _g.end());
      }

      /**
       * isBool. Check if a variable is Boolean type.
       * param[in] Expr var
       * return bool
       */
      static bool isBool (Expr var)
      {
        if (!(isOpX<VALUE>(var) ||
              isOpX<VARIANT>(var)))
          return false;
        Expr u = var;
        if (isOpX<VARIANT>(u)) u = variant::mainVariant(u);
        if (isOpX<VARIANT>(u)) u = variant::mainVariant(u);
        if (!isOpX<VALUE>(u)) return true;

        if (isBoolType(getTerm<const Value*>(u)->getType()))
          return true;

        return false;
      }
    };

    typedef NumericCons<TerminalAssert> UFO_TERMINAL;
    typedef BasicExprConverter<UFO_TERMINAL> ExprConverter;
  }
}

namespace ufo 
{
  typedef PkApron<Expr, apron::ExprConverter> ExprApron;
  //typedef BoolApronProduct<Expr, apron::ExprConverter> ExprApron;
  typedef BoolPkProduct<Expr, apron::ExprConverter> ExprApronPk;
  typedef BoolOctProduct<Expr, apron::ExprConverter> ExprApronOct;
  typedef BoolBoxProduct<Expr, apron::ExprConverter> ExprApronBox;
}

#endif
