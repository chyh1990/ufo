#ifndef __DNF_BOUND_HPP
#define __DNF_BOUND_HPP

#include "ufo/Asd/Bnd/Bound.hpp"
#include "ufo/Asd/Apron/ExprApron.hpp"
#include "ufo/Smt/ExprZ3.hpp"
#include "ufo/Asd/Interval.hpp"
#include "ufo/Smt/MUS.hpp"

using namespace std;
using namespace boost;

namespace ufo
{
  /**
   * DNFBound.
   * Bound variables in a first order logic formula with linear
   * constraints by constructing an over-approximated
   * Disjunctive Normal Form formula incrementally.
   */
  template <typename AP>
  class DNFBound : public Bound
  {
  public:
    DNFBound (ExprFactory &_e, const ExprSet &_v, Expr _p) :
      phi (_p),
      sphi(deepSimp(_p)),
      efac(_e), varbase(_v),
      realVars(filterVars(varbase,true)),
      boolVars(filterVars(varbase,false)),
      allVars(filterVars(collectVars(phi),true)),
      literals(collectLiterals(sphi)),
      ap(efac, allVars)
    {
      forall (Expr v, varbase)
        bounds[v] = Interval::bot();
    }

    ~DNFBound () {}

    /**
     * scanCons.
     * Collect all the linear terms from formula phi.
     * param[in] Expr phi.
     */
    // void scanCons (Expr phi)
    // {
    //   filter (phi, ConsFilter(), inserter(consbase, consbase.begin()));

    //   // Rewrite ITEs
    //   for (set<Expr>::iterator it = consbase.begin();
    //        it != consbase.end();)
    //   {
    //     assert (isOp<ComparissonOp> (*it));
    //     if (isOpX<EQ> (*it) && isOpX<ITE>((*it)->right()))
    //     {
    //       Expr Tpart = mk<EQ> ((*it)->left(), (*it)->right()->right());
    //       Expr Epart = mk<EQ> ((*it)->left(), *(((*it)->right()->args_begin())+2));
          
    //       consbase.erase (it++);
    //       consbase.insert (Tpart);
    //       consbase.insert (Epart);
    //     }
    //     else
    //       ++it;
    //   }
    // }

    /**
     * boundAll.
     * Bound all variables.
     */
    void boundAll (Expr phi)
    { 
      //Bound Boolean variables
      boundBool (phi, boolVars, bounds);
      
      ExprZ3 z3 (efac, StringMap(), true);
      z3.push();
      z3.assertExpr (phi);
      
      while (z3.solve())
      {           
        // Create an apron state from constraints satisfied by the model
        ExprVector mE = getMinimizedE (z3, literals, phi);
        // Simplify constraints in mE
        ExprVector sE = simplifyConstraints (mE);
        
        ap_ptr s = ap.cons2state (sE);

        ExprVector g;
        foreach (Expr _v, realVars)
        { 
          Interval _b;
          // Some variables from the hints do not appear in the formula
          if (allVars.count (_v) <= 0)
            _b = Interval::top();
          else
            // Get Box from apron
            _b = ap.getBound (_v, s);

          // Join with the current Box
          bounds[_v] = bounds[_v].join (_b);
          g.push_back (_b.gamma(_v));
        }

        Expr _gamma = mknary<AND>(mk<TRUE>(efac), g.begin(), g.end());
        // block the region inside the Box
        z3.assertExpr (mk<NEG> (_gamma));
      }
      z3.pop();
    }

    /**
     * Minimize E
     * param[in] ExprZ3 &_z3: Z3 object
     * param[in] ExprSet &lits: set of literals in the formula
     * param[in] Expr _phi: deep simplified phi
     * return ExprVector: a minimized set of linear constraints satisfied
     * by the current model
     */
    inline ExprVector getMinimizedE (ExprZ3 &_z3, ExprSet &lits, Expr _phi)
    {
      Expr trueE = mk<TRUE> (efac);
      
      ExprVector E; // Set of literals satisfied by the current model
      forall (Expr _l, lits)
      {
        if (isOpX<TRUE> (_z3.getModelValue (_l)))
          E.push_back (_l);
      }

      // Assert (/\E => phi)
      assert (!z3_is_sat (boolop::land
                          (mknary<AND> (mk<TRUE> (efac),
                                        E.begin(), E.end()),
                           boolop::lneg(_phi))));

      /*
      // Debug simplify
      ExprVector Econs = filterLinearCons (E);
          
      Expr cjE = mknary<AND> (trueE, Econs.begin(), Econs.end());
      errs () << "E: " << *cjE << "\n";
      Expr simE = z3_simplify (cjE);
      errs () << "simp: " << *simE << "\n";
      */
      
      // Construct assumptions from E
      ExprVector assumps; // Assumptions: ASM<e> => e
      forall (Expr _e, E)
        assumps.push_back (mk<IMPL>(mk<ASM>(_e), _e));

      Expr lphi = boolop::land (boolop::lneg(_phi), 
                                mknary<AND> (trueE,
                                             assumps.begin(), assumps.end()));

      // Minimize E
      ExprSet usedAssumps; // Used assumptions        
      ExprZ3 mus_z3 (efac);
      mus_basic (mus_z3, lphi, std::inserter (usedAssumps,
                                              usedAssumps.begin()),
                 3);

      // Extract linear constraints from assumptions
      ExprVector newCons = filterLinearCons (usedAssumps); // Used constraints

      return newCons;
    }

    /**
     * Simplify Constraints
     * Discover equalities from pair of inequalities.
     * e.g. x>=k and x<=k  -->  x==k
     * Substitute all discovered equalities to constraints
     */
    inline ExprVector simplifyConstraints (const ExprVector &_cons)
    {
      // x>=k /\ x<=k => x=k
      // Eliminate dimensions
      ExprVector resCons;      
      ExprMap geq, leq, eqk;
      forall (Expr c, _cons)
      {
        Expr left = c->left();
        Expr right = c->right();
        // x == k
        if (isOpX<EQ> (c) && isOpX<MPQ>(right) && isOpX<VARIANT>(left))
          eqk[left] = right;
        // x >= k
        else if (isOpX<GEQ> (c) && isOpX<MPQ>(right) && isOpX<VARIANT>(left))
        {
          if (leq.count (left) > 0 && leq[left] == right)
            eqk[left] = right;
          else geq[left] = right;
        }
        // x <= k
        else if (isOpX<LEQ> (c) && isOpX<MPQ>(right) && isOpX<VARIANT>(left))
        {
          if (geq.count (left) > 0 && geq[left] == right)
            eqk[left] = right;
          else leq[left] = right;
        }
        else
          resCons.push_back (c);
      }

      // Substitute equalities
      for (vector<Expr>::iterator c = resCons.begin();
           c != resCons.end(); ++c)
        *c = replace (*c, eqk);        

      // Adding equalities into constraints
      forall (ExprPair e, eqk)
        resCons.push_back (mk<EQ>(e.first, e.second));
      forall (ExprPair e, leq)
        resCons.push_back (mk<LEQ>(e.first, e.second));
      forall (ExprPair e, geq)
        resCons.push_back (mk<GEQ>(e.first, e.second));
      
      return resCons;
    }

    /**
     * Extract linear constraints
     * param[in] R &collection: input is a collection of literals/ASM<literals>
     * return ExprVector: a vector of linear constraints
     */
    template <typename R>
    ExprVector filterLinearCons (R &collection)
    {
      ExprVector res;
      forall (Expr _c, collection)
      {
        Expr lit = _c;
        if (isOpX<ASM> (lit)) lit = _c->left();
        if (isOpX<NEG> (lit) && isOp<ComparissonOp> (lit->left()))
        {
          res.push_back (flip (lit->left()));
          continue;
        }
        else if (isOp<ComparissonOp> (lit))
          res.push_back (lit);
      }
      return res;
    }
    
    /**
     * Override [] operator
     */
    inline Interval operator [] (Expr var)
    {
      assert (bounds.count (var) > 0);
      return bounds[var];
    }
    
  private:
    Expr phi; // original phi
    Expr sphi; // simplified phi
    ExprFactory &efac;
    const ExprSet &varbase;
    ExprSet realVars; // real variables need to be bounded
    ExprSet boolVars; // Boolean variables need to be bounded
    ExprSet allVars; // All real-valued variables appeared in the formula
    ExprSet literals; // All literals in the formula
    VarBounds bounds; // temporary bounds for each variable
    AP ap;
  };
}

#endif
