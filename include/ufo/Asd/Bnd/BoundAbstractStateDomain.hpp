#ifndef __BOUND_ABSTRACT_DOMAIN_HPP__
#define __BOUND_ABSTRACT_DOMAIN_HPP__

#include "ufo/ufo.hpp"


#include "ufo/Asd/Interval.hpp"
#include "ufo/Asd/AbstractStateDomain.hpp"
#include "ufo/Asd/Bnd/VarBound.hpp"

namespace ufo
{

  void print(Expr e) __attribute__((externally_visible,used,noinline));
  void print(Interval e);
  void print(AbstractState abs);




  class BoundAbstractStateValue : public AbstractStateValue
  {
    protected:
      VarBounds val;
    public:
      BoundAbstractStateValue (VarBounds &_val) : val(_val) {} 

      VarBounds& getVal () { return val; }
  };

  class BoundAbstractStateDomain : public AbstractStateDomain
  {
  private:
    LBE& lbe;
    ExprFactory &efac;
    std::map<CutPointPtr, ExprSet> cp2vars;
    std::map<Expr, Expr> evar2var;
    ExprSet hints;
    COICache<Expr, mpq_class> cache;

    AbstractState topV;
    AbstractState botV;
    typedef std::pair<Expr, Interval> ExpIntPair;

  public:
    BoundAbstractStateDomain (LBE& _LBE, ExprFactory& _efac) :
      lbe(_LBE), efac(_efac) 
    {
      VarBounds topMap;
      topV = AbstractState (new BoundAbstractStateValue (topMap));

      VarBounds botMap;
      botV = AbstractState (new BoundAbstractStateValue (botMap));
    }

  protected:
    bool isTopV (AbstractState x) const { return x == topV; }
    bool isBotV (AbstractState x) const { return x == botV; }

    AbstractState getTopV () const { return topV; }
    AbstractState getBotV () const { return botV; }


  public:
    Interval&  getBounds (AbstractState v, Expr var) const;

    VarBounds&  getValue (AbstractState v) const
    {
      assert (!(isTopV(v) || isBotV(v)));
      return dynamic_cast<BoundAbstractStateValue*>(&*v)->getVal ();
    }


    Expr gamma (Expr v, Interval &iv)  { return iv.gamma(v);}

    Expr gamma (Loc loc, AbstractState v) 
    {
      if (isTopV(v)) return mk<TRUE>(efac);
      if (isBotV(v)) return mk<FALSE>(efac);

      Expr res = mk<TRUE> (efac);

      forall (ExpIntPair vb, getValue(v))
	res = boolop::land (res, gamma (vb.first, vb.second));

      return res;
    }

    /** Add a variable to a cut point */
    void addVar (CutPointPtr cp, Expr v) 
    { 
      if (cp2vars[cp].insert (v).second)
	errs () << "NEW VAR " << *v 
		<< " to " << cp->getBB ()->getName () << "\n";
    }

    //** Add variables to a cut point */
    void addVars (CutPointPtr cp, ExprVector v)
    {
      cp2vars[cp].insert (v.begin(), v.end());
      forall (Expr var, v)
	errs() << "NEW VAR " << *var
	       << " to " << cp->getBB ()->getName () << "\n";
    }

    /** Returns variables for a given cutpoint */
    const ExprSet& getVars(CutPointPtr cp) { return cp2vars [cp]; }

    /** Forgets all variables */
    void reset () { cp2vars.clear(); }

    /**
       RefinementHint
       \Input: location, hint
       \Output: N.A
       \Side Effect: adding variables/terms to the domain
    */
    virtual void refinementHint (CutPointPtr loc, Expr hint);

    virtual bool isLeq (CutPointPtr loc, AbstractState v1, AbstractState v2) 
    {
      if (isTopV(v2))
	return true;
      else if (isTopV(v1))
	return false;

      if (isBotV(v1))
	return true;
      else if (isBotV(v2))
	return false;
      //assert (getValue(v1).size() == getValue(v2).size());
      forall (Expr var, getVars(loc))
	// ! (v1 <= v2)
	if (!(getBounds(v1, var) <= getBounds(v2, var))) return false;
      return true;
    }

    virtual bool isLeq (CutPointPtr loc, AbstractState v1, 
			AbstractStateVector const &u)
    {
      // Approximation: return true if v1 isLeq anyone of u
      forall (AbstractState u1, u)
	if (isLeq (loc, v1, u1)) return true;
      return false;
    }

    virtual bool isEq (CutPointPtr loc, AbstractState v1, AbstractState v2) 
    {
      if ((isTopV(v1) && isTopV(v2)) ||
	  (isBotV(v1) && isBotV(v2)))
	return true;
      if ((isTopV(v1) && isBotV(v2)) ||
	  (isBotV(v1) && isTopV(v2)))
	return false;

      forall (Expr var, getVars(loc))
	if (!(getBounds(v1, var) == getBounds(v2, var))) return false;

      return true;
    }

    virtual AbstractState join (CutPointPtr loc, 
				AbstractState v1, AbstractState v2)
    {
      if (isTopV (v1) || isTopV (v2)) return getTopV ();
      if (isBotV (v1)) return v2;
      if (isBotV (v2)) return v1;

      VarBounds vbs;
      forall (Expr var, getVars(loc))
        {
          vbs[var] = getBounds(v1, var).join(getBounds(v2, var));
        }
      return absState (vbs);
    }

    AbstractState absState (VarBounds &vbs)
    {
      if (isTopVbs (vbs)) return getTopV ();
      if (isBotVbs (vbs)) return getBotV ();

      forall (ExpIntPair vb, vbs)
        {
          assert (!isWrong(vb.second.first));
          assert (!isWrong(vb.second.second));
        }

      return AbstractState (new BoundAbstractStateValue (vbs));
    }

    bool isTopVbs (VarBounds &vbs)
    {
      forall (ExpIntPair kv, vbs) if (!kv.second.isTop ()) return false;
      return true;
    }

    bool isBotVbs (VarBounds &vbs)
    {
      forall (ExpIntPair kv, vbs) if (kv.second.isBot ()) return true;
      return false;
    }


    virtual AbstractState meet (CutPointPtr loc, 
				AbstractState v1, AbstractState v2)
    {
      //assert (getValue(v1).size() == getValue(v2).size());
      if (isBotV(v1) || isBotV(v2)) return getBotV();
      if (isTopV(v1)) return v2;
      if (isTopV(v2)) return v1;

      VarBounds vbs;
      forall (Expr var, getVars(loc))
        {
          vbs[var] = getBounds(v1, var).meet(getBounds(v2, var));
        }
      return absState (vbs);
    }

    virtual AbstractState widen (CutPointPtr loc, 
				 AbstractState v1, AbstractState v2)
    {
      if (isTopV(v2)) return getTopV();
      if (isBotV(v2)) return getBotV();

      //assert (getValue(v1).size() == getValue(v2).size());
      VarBounds vbs;
      forall (Expr var, getVars(loc))
        {
          vbs[var]= getBounds(v1, var).widen(getBounds(v2, var));
        }
      return absState(vbs);
    }

    virtual AbstractState widenWith (Loc loc, AbstractStateVector const &oldV, 
				     AbstractState newV)
    {
      AbstractState v;
      AbstractState u;
      bool flagv = false;

      rev_forall (AbstractState x, oldV)
        {
          if (flagv && isLeq (loc, x, v)) continue;

          AbstractState w = join (loc, x, newV);

          if (!flagv || isLeq (loc, w, u))
	    {
	      flagv = true;
	      v = x;
	      u = w;
	    }
          else break;
        }

      return widen (loc, v, u);
    }

    virtual AbstractState post (AbstractState pre, 
				CutPointPtr src, CutPointPtr dst)
    {
      assert (0 && "Unreachable");
      //return exprPost (gamma (src, pre), src, dst); 
    }

    // To test if an AbstractState contains 0/0 as bound.
    bool isBadState (AbstractState v)
    {
      if (isTopV(v) || isBotV(v))
	return false;
      forall (ExpIntPair vb, getValue(v))
        {
          if( isWrong(vb.second.first) || isWrong(vb.second.second))
            return true;
        }
      return false;
    }

    virtual AbstractState post (const LocLabelStatePairVector &pre,
				Loc dst, 
				BoolVector &deadLocs);

    bool isFinite () { return false; }

    AbstractState alpha (CutPointPtr loc, Expr val) 
    { 
      assert (0 && "Unreachable");
      return AbstractState ();
    }

    AbstractState top (CutPointPtr loc) 
    {
      return getTopV();
    }

    AbstractState bot (CutPointPtr loc)
    {
      return getBotV();
    }

    bool isTop (CutPointPtr loc, AbstractState v)
    {
      return isTopV(v);
    }

    bool isBot (CutPointPtr loc, AbstractState v)
    {
      return isBotV(v);
    }

  protected:
    // Actual post calculation based on varBound.hpp
    AbstractState exprPost (Expr pre, CutPointPtr src, CutPointPtr dst);
  /** Unevaluated the variable/term: from VARIANT to VALUE */
    Expr uneval (Expr e)
    {
      if (isOpX<VARIANT>(e))
	{
	  assert (evar2var.count(e) > 0);
	  return evar2var[e];
	}
      else return replace (e, evar2var);
    }

  };
}

#endif
