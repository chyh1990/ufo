#ifndef __EDGE_COMP_H_
#define __EDGE_COMP_H_

#include <map>

#include <llvm/Support/CFG.h>

#include "ufo/ufo.hpp"
#include "ufo/Arg.hpp"

#include "ufo/UfoCL.hpp"

namespace ufo
{  
  class Environment;
  
  namespace detail
  {
    struct EvalVisitor
    {
      Environment &env;

      EvalVisitor (Environment &e) : env (e) {}
      inline VisitAction operator () (Expr exp) const;
    };
    
    inline Expr mkEdgVar (Expr src, Expr dst)
    {
      /* the tuple (dst, src) is reversed, but this is how this code
	 was written. Not changing now to preserve possibly expected
	 behavior   */
      return bind::boolVar (mk<TUPLE> (dst, src));
    }
    
    
  }
  
  /** An environment for interpreting formulas. 
      Maps between llvm::Value and expr::Expr 
 */
  class Environment
  {
  public:
    typedef std::map<Expr, Expr> map_type;

  private:

    /** environment map */
    map_type env;
    
    /** global map for fresh names */
    typedef boost::shared_ptr<map_type> genv_type;
    genv_type genv;

    ExprFactory *efac;

    detail::EvalVisitor evalVisitor;
  
  public:

    
    typedef map_type::iterator iterator;
    typedef map_type::const_iterator const_iterator;
	
    Expr toExpr (const Value &v) const { return mkTerm (&v, *efac); }
    Expr toExpr (const BasicBlock &v) const { return mkTerm (&v ,*efac); }
    Expr toExpr (const Expr &v) const { return v; }
    Expr toExpr (const std::string &v) { return mkTerm (v, *efac); }
    
    Environment(ExprFactory& e): genv(new map_type ()), efac(&e), 
				 evalVisitor (*this) {}
    
    Environment (const Environment &other) 
      : env (other.env), 
      genv(other.genv), 
      efac (other.efac), 
      evalVisitor(*this) 
	{}

    /** noarg constructor for containers */
    Environment() : efac(NULL), evalVisitor (*this) {}
    
    
    Environment& operator=(const Environment& other)
    {
	Environment tmp(other);
	std::swap (tmp.env, env);
	std::swap (tmp.genv, genv);
	efac = tmp.efac;

	return *this;
      }
	

    ExprFactory &getExprFactory () { return *efac; }

    /** Evaluates the expression in the given environment */
    Expr eval (Expr exp)
    {
      return dagVisit (evalVisitor, exp);
    }
    

    template <typename T> bool isBound (const T& v) const
    {
      return (env.find (toExpr (v)) != env.end());
    }


    iterator begin () { return env.begin (); }
    iterator end () { return env.end (); }

    const_iterator begin () const { return env.begin (); }
    const_iterator end  () const { return env.end (); }

    void erase (iterator i) { return env.erase (i); }
    size_t erase (const Expr &key) { return env.erase (key); }
     

    /** remove all bindings */
    void clear () { env.clear (); }
    
    
    bool has_binding (const Expr v, const Expr u) const
    {
      map_type::const_iterator it = env.find (v);
      return it != env.end () && it->second == u;
    }

    // -- lookup and refresh if needed
    template <typename T>
    Expr lookup(const T& v)
    {
      Expr expr = toExpr (v);
      map_type::iterator it = env.find (expr);
      if (it == env.end ()) 
	return refresh (expr);
      
      return it->second;
    }
    
    void printEnv() const 
    {
      errs() << "Printing Env\n\n";
      for (map_type::const_iterator it = env.begin(); it != env.end(); ++it){
	errs() << *it->first << " ===> " << *it->second << "\n";
      }
    }

    template <typename T>
    void bind (const T& v, const Expr &u)
    {
      //check if first bound expression is a variant
      assert (isOpX<VARIANT> (u));
      env [toExpr(v)] = u;
    }

    template <typename T>
    Expr refresh (const T& _v)
    {
      Expr v = toExpr (_v);
            
      map_type::iterator geit = genv->find (v);
        
      int idx = 0;
      if (geit != genv->end ())
	{
	  idx = variant::variantNum (geit->second) + 1;
	  // -- genv always maps v to 'variant of v'
	  assert (variant::mainVariant (geit->second) == v);
	}
      
      Expr u = variant::variant (idx, v);
     
      (*genv)[v] = u;
      bind (v, u);
      
      return u;
    }


    
    /**
     * Unifies (intersects) this environment with a collection of
     * environments. Refreshed keys are returned in refreshed set.
     */
    template <typename Iterator> 
    void unify (Iterator begin, Iterator end, ExprSet &refreshed)
    {
      for (Iterator oit = begin; oit != end; ++oit)
	{
	  Environment* oEnv = *oit;
	  
	  map_type::iterator me = env.begin ();
	  while (me != env.end ())
	    {
	      Expr key = me->first;
	      
	      map_type::iterator other = oEnv->env.find (key);

	      // -- not found, not in the intersection
	      if (other == oEnv->env.end ())
		{
		  env.erase (me++);
		  refreshed.erase (key);
		  continue;
		}

	      // -- key is in both me and other, check value
	      if (me->second != other->second)
		// -- needs refresh
		refreshed.insert (key);
	      ++me;
	    }	  
	}
    
      // -- refresh anything that needs refresh
      for (ExprSet::const_iterator it = refreshed.begin (), 
	     end = refreshed.end (); it != end; ++it)
	refresh (*it);
      
    }
    
  };


  class SEdgeCondComp
  {
  protected:
    ExprFactory& efac;
    Environment& env;
    SEdgePtr edge;
    Expr cond;
    
    /* edge.getSrc */
    const CutPoint &src;
    /* edge.getDst */
    const CutPoint &dst;

    Expr trueE;
    Expr falseE;
    LBE& lbe;

    // -- true if compute() was called and condition is available
    bool computed;
    
    std::vector<Expr> assumptions;   
    
    // -- compute formula with assumptions
    bool withAssume;

    static unsigned long asmCount;

    /** true if pointers are tracked in VC. Controlled by
	--ufo-track-ptr command line option. */
    bool trackPtr;

  private:
    SEdgeCondComp (const SEdgeCondComp &other) 
      : efac(other.efac), env(other.env), src(other.src), dst(other.dst),
	lbe(other.lbe)
      { assert (0);  }
    
  protected:
    /** wraps an expression in a unique assumption */
    Expr mkAssume (Expr e)
    {
      // -- don't add assumptions to constants, and when they are not needed
      if (e == trueE || /*e == falseE ||*/ !withAssume) return e;
      
      // -- add a unique assumption. 
      /* XXX the asmCounter keeps growing and may overflow */
      return mk<IMPL> (mk<ASM> (mkTerm (asmCount++, efac)), e);
    }

  public:
    SEdgeCondComp (ExprFactory &f, 
		   Environment& e, 
		   SEdgePtr edg, 
		   LBE &_lbe, bool assum = false
                   ) :
    efac(f), env(e), 
      edge(edg), src(*edg->getSrc()), 
      dst(*edg->getDst()), trueE (mk<TRUE> (efac)), falseE(mk<FALSE>(efac)),
    lbe(_lbe), computed(false), withAssume(assum), 
    trackPtr (ufocl::UFO_TRACK_PTR)
      { updateEnvToFirstInst (env, src); }
    
    /**
     * Updated Environment from the begining of the basic block, to
     * the environment at the first non-PHI instruction of the block.
     * Idempotent.
     */
    static Environment &updateEnvToFirstInst (Environment &env, 
					      const CutPoint &cp) 
    {
      ExprFactory &efac = env.getExprFactory ();
      
      for (BasicBlock::const_iterator it = cp.getBB ()->begin(), 
	     end = cp.getBB()->end (); it != end; ++it) 
	{
	  // -- stop at first non-phi node
	  if (!isa<PHINode> (*it)) break;
	  
	  // map phi to what phi' is pointing to
	  const Value *phi = it;
	  Expr u = env.lookup (variant::prime (mkTerm (phi, efac)));
	  env.bind (*phi, u);
	}
      return env;
    }
    
    Expr getPhiArg (const BasicBlock* bb, const PHINode& phi) const
    {
      return lookup (phi.getIncomingValueForBlock (bb));
    }

    Expr getPhiRes(const Instruction& i) { return env.lookup (i); }

    Expr edgeCondition (const BasicBlock* sr, const BasicBlock* ds)
    {
      ExprVector res;
      for (BasicBlock::const_iterator it = sr->begin (), 
	     end = sr->end (); it != end; ++it)
	{
	  // -- skip PHI nodes
	  if (isa<PHINode> (*it)) continue;
	  
	  if (const TerminatorInst *ti = dyn_cast<const TerminatorInst> (it))
	    {
	      const BranchInst* bi = dyn_cast<const BranchInst> (ti);

	      // -- last instruction in the bb, nothing to do
	      if (bi == NULL) break;

	      // -- uncoditional branch, nothing to do
	      if (!bi->isConditional ()) break;
	      
	      // -- if the branching condition is true/false
	      if (isa<ConstantInt> (bi->getCondition()))
		{
		  const ConstantInt* cint = 
		    dyn_cast<const ConstantInt>(bi->getCondition());
		  if (cint->isOne () && bi->getSuccessor (0) != ds)
		    return falseE;
		  else if (cint->isZero () && bi->getSuccessor (1) != ds)
		    return falseE;

		  // -- constant true, ignore
		  continue;
		}

	      const Instruction* c = dyn_cast<Instruction>(bi->getCondition ());
	      assert (c != NULL);
	      res.push_back (bi->getSuccessor (0) == ds ? env.lookup (*c) : 
			     mk<NEG> (env.lookup (*c)));

	      // -- last instruction
	      break;
	    }

	  // -- call instruction
	  if (const CallInst *ci = dyn_cast<const CallInst> (it))
	    {
	      Function *Callee = ci->getCalledFunction ();
	      if (Callee == 0) continue;
	      
	      // -- skip if not the assume function
	      if (!Callee->getName ().equals ("__VERIFIER_assume")) continue;

	      // -- the condition
	      Value *c = ci->getArgOperand (0);
	      
	      // -- remove zext that is introduced to cast boolean
	      // -- value to the argument of __VERIFIER_assume
	      if (const ZExtInst *ze = dyn_cast<const ZExtInst> (c))
		c = ze->getOperand (0);
	      
	      // -- if the branching condition is true/false
	      if (isa<ConstantInt> (c))
		{
		  const ConstantInt* cint = dyn_cast<const ConstantInt>(c);
		  if (cint->isZero ()) return falseE;
		  continue;		  
		}

	      if (!isBoolType (c->getType ()))
		errs () << "type of " << *c 
			<< " is " << *(c->getType ()) << "\n";
		  
	      assert (isBoolType (c->getType ()));

	      res.push_back (env.lookup (*c));
	    }
	}
      return mknary<AND> (trueE, res.begin (), res.end ());
    }

    // -- replaced by above. to be removed
    // Expr edgeCondition (const BasicBlock* sr, const BasicBlock* ds)
    // {
    //   // -- look at the branch condition of sr
    //   const TerminatorInst* ti = sr->getTerminator();
	
    //   const BranchInst* bi = dyn_cast<const BranchInst>(ti);
      
    //   if (bi == NULL) return trueE;

    //   if (!bi->isConditional ()) return trueE;
      
    //   // -- if the branching condition is true/false
    //   if (isa<ConstantInt>(bi->getCondition()))
    // 	{
    // 	  const ConstantInt* ci = 
    // 	    dyn_cast<const ConstantInt>(bi->getCondition());
    // 	  if (ci->isOne ())
    // 	    return bi->getSuccessor (0) == ds ? trueE : falseE;
    // 	  return bi->getSuccessor (0) == ds ? falseE : trueE;
    // 	}

    //   const Instruction* c = dyn_cast<Instruction>(bi->getCondition ());
      
    //   Expr res = bi->getSuccessor (0) == ds 
    // 	? env.lookup (*c) : mk<NEG> (env.lookup (*c));      

    //   return res;
    // }
      
    Expr doLastControl ()
    {
      const BasicBlock* bb =  dst.getBB ();

      // XXX This might be necessary for a self-loop when src and
      // XXX destination block are the same.
      // XXX At the moment, doLastControl does not use bb variable at all

      // -- XXX should this hold?
      // env.refresh(*bb);
      
      // -- for every PHINode
      for (BasicBlock::const_iterator it = bb->begin(), 
	     end = bb->end (); it != end; ++it) 
	{
	  // -- stop at first non-phi node
	  if (!isa<PHINode> (*it)) break;
	  const Value* phi = it;

	  Expr u = env.refresh (variant::prime (mkTerm (phi, efac)));
	}  
	
      ExprVector preCond;
      ExprVector evars;

      // -- for every predecessor
      for (const_pred_iterator PI = pred_begin(bb), 
	     E = pred_end(bb); PI != E; ++PI) 
	{
	  const BasicBlock *pred = *PI;
	
	  if (!lbe.canImmReach (src, pred)) continue;
	  if (pred == bb && src != dst) continue;
          
	  if (lbe.isCutPoint (pred) && pred != src.getBB ())
	    continue;
          
	  Expr condc = env.lookup (*pred);

	  // XXX in the vase of the self-loop, mkTerm(bb,efac) might
	  // be used to repesent the basic block at the source. Need
	  // to refresh or use a prime to indicate the last block!

	  /* XXX uncomment to add unique-predecessor-constraint. 
	     XXX For some reason, does really poorly on token_ring.04
	     XXX more investigation is needed
	  */
	  /* // -- don't have variable for bb, so re-using bb itself. */
	  /* // -- condc should be enough to guarantee uniquieness */
	  /* Expr edgeV = detail::mkEdgVar (conc, mkTerm (bb, efac)); */
	  /* condc = boolop::land (condc, edgeV); */
	  /* evars.push_back (edgeV); */

	  condc = boolop::land (condc, edgeCondition (pred, bb));

	  for (BasicBlock::const_iterator it = bb->begin(), 
		 end = bb->end (); it != end; ++it) 
	    {
	      if (!isa<PHINode>(*it)) break;


	      if (it->getType ()->isIntegerTy () || 
		  (trackPtr && it->getType ()->isPointerTy ()))
		{
		  const PHINode *phi = dyn_cast<const PHINode> (&*it);
		  assert (phi != NULL);
		  
		  Expr phiRes = env.lookup 
		    (variant::prime(mkTerm<const Value*> (phi, efac)));

		  Expr phiArg = getPhiArg (pred, *phi);

		  if (isBoolType(it->getType())){
		    condc = boolop::land (condc, mk<IFF>(phiRes, phiArg));
		  }else{
		    // -- add condition from phinode with respect to pred
		    //condc = boolop::land (condc, );
		    Expr phiE = mk<EQ>(phiRes, phiArg);
		    phiE = mk<AND> (mk<LEQ> (phiRes, phiArg),
				    mk<GEQ> (phiRes, phiArg));
		    condc = boolop::land (condc, phiE);
		  }
		}
	    }

	  if (condc != trueE) preCond.push_back(condc);
	}

      Expr res = mknary<OR> (trueE, preCond.begin (), preCond.end ());
      if (!evars.empty ())
      	res = boolop::land (res, exactlyOne (evars, efac));
			    
      return res;
    }

    Expr doControl (const BasicBlock* bb)
    {
      // -- refresh variable for this block
      Expr bbVar = env.refresh (*bb);
      
      // skip entry block and blocks with no predecessors
      if (bb == src.getBB() || pred_begin(bb) == pred_end(bb))
	return trueE;
	
      // -- refresh all PHINodes
      for (BasicBlock::const_iterator it = bb->begin(),
	     end = bb->end (); it != end; ++it) 
	{
	  if (!isa<PHINode>(*it)) break;

	  // -- refreshes phi nodes
	  env.refresh (*it);
	}
	
      ExprVector preCond;

      ExprVector evars;

      // -- for all predecessors of bb
      for (const_pred_iterator PI = pred_begin (bb), 
	     E = pred_end(bb); PI != E; ++PI) 
	{
	  const BasicBlock *pred = *PI;
	  
	  // -- check whether pred is on the src to dst edge
	  if (!lbe.canImmReach (src, pred)) continue;
	  if (lbe.isCutPoint (pred) && pred != src.getBB ()) continue;
	  

	  Expr condc = env.lookup(*pred);
	  
	  // -- create an edge variable
	  Expr edgeV = detail::mkEdgVar (condc, bbVar);
	  condc = boolop::land (condc, edgeV);
	  evars.push_back (edgeV);
	  

	  condc = boolop::land (condc, (edgeCondition(pred, bb)));
	  
	  // -- for every PHINode
	  for (BasicBlock::const_iterator it = bb->begin(), 
		 end = bb->end (); it != end; ++it) 
	    {
	      if (!isa<PHINode>(*it)) break;
	      
	      // -- only attempt to approximate integer registers and
	      // -- pointers (which we treat as integer addresses,
	      // -- i.e., never dereferenced
	      if (it->getType ()->isIntegerTy () || 
		  (trackPtr && it->getType ()->isPointerTy ()))
		{
		  const PHINode *phi = dyn_cast<const PHINode> (&*it);
		  assert (phi != NULL);
		  
		  Expr phiArg = getPhiArg(pred, *phi);
		  Expr phiRes = getPhiRes (*phi);
		  //add condition from phinode with respect to pred
		  if (isBoolType(it->getType()))
		    condc = boolop::land (condc, mk<IFF>(phiRes, phiArg));
		  else
		    {
		      Expr phiE = mk<EQ>(phiRes, phiArg);
		      phiE = mk<AND> (mk<LEQ> (phiRes, phiArg),
				      mk<GEQ> (phiRes, phiArg));
		      condc = boolop::land (condc, mkAssume (phiE));
		    }
		  
		}
	    }
	  if (condc != trueE) preCond.push_back (condc);
	}
        
      Expr res = boolop::limp (bbVar, 
			       mknary<OR> (trueE,
					   preCond.begin (), preCond.end ()));
      // -- if have predecessors, say that exactly one must be taken
      if (!evars.empty ())
      	res = boolop::land (res,
      			    boolop::limp (bbVar,
      					  exactlyOne (evars, efac)));
      return res;      
    }

    Expr lookup (const Value *v) const
    {
      assert (v != NULL);
      
      if (isa<ConstantInt>(v))
	{
	  const ConstantInt *ci = cast<const ConstantInt>(v);
	  if (ci->getBitWidth() == 1)
	    return ci->isOne () ? trueE : falseE;
          
	  mpq_class k = toMpz (ci->getValue ());
          return mkTerm (k, efac);
	}
      else if (isa<ConstantPointerNull>(v))
	return mkTerm (mpq_class (0), efac);
      // -- any other constant, is represented by symbolic value
      else if (isa<Constant> (v)) return mkTerm (v, efac);
      
      return env.lookup (*v);
    }


    Expr doLogic(const Expr &lhs, const Instruction &i)
    {
      Value* v1 = i.getOperand(0);
      Value* v2 = i.getOperand(1);
      
      const IntegerType* t1 = dyn_cast<const IntegerType>(v1->getType());
      const IntegerType* t2 = dyn_cast<const IntegerType>(v2->getType());

      // -- operation not over Integer type -- shouldn't happen
      if (t1 == NULL || t2 == NULL) return trueE;

      if (t1->getBitWidth() != 1 || t2->getBitWidth() != 1)
	// -- cannot handle such operators on integers
	// XXX add workaround for shifts with constant values
	return trueE;


      Expr op1;
      Expr op2;
      
      ConstantInt* c1;
      ConstantInt* c2;

      // -- if operands are constants, get their values
      // -- otherwise, resolve in the environment
      if ((c1 = dyn_cast<ConstantInt>(v1)) != NULL)
	op1 = c1->isOne () ? trueE : falseE;
      else
	op1 = env.lookup (*v1);

      if ((c2 = dyn_cast<ConstantInt>(v2)) != NULL)
	op2 = c2->isOne () ? trueE : falseE;
      else
	op2 = env.lookup (*v2);


      switch(i.getOpcode())
	{
	case BinaryOperator::And:
	  return mk<IFF>(lhs, mk<AND>(op1,op2));
	case BinaryOperator::Or:
	  return mk<IFF>(lhs, mk<OR>(op1,op2));
	case BinaryOperator::Xor:
	  return mk<IFF>(lhs, mk<XOR>(op1,op2));
	
	case BinaryOperator::Shl:
	case BinaryOperator::AShr:
	case BinaryOperator::LShr:  
	  //XXX handle shifts
	default:
	  return trueE;
	}
    }
    
    Expr doArith(const Expr &lhs, const Instruction &i)
    {
      Value* v1 = i.getOperand(0);
      Value* v2 = i.getOperand(1);


      Expr op1 = lookup (v1);
      Expr op2 = lookup (v2);
      
      switch(i.getOpcode())
	{
	case BinaryOperator::Add:
	  return mk<EQ>(lhs ,mk<PLUS>(op1, op2));
	case BinaryOperator::Sub:
	  return mk<EQ>(lhs ,mk<MINUS>(op1, op2));
	case BinaryOperator::Mul:
	  return mk<EQ>(lhs ,mk<MULT>(op1, op2));
	case BinaryOperator::UDiv:
	  return trueE;
	  // XXX why the check?
          if (!(isa<ConstantInt>(v1) || isa<ConstantInt>(v2))) return trueE;
	  //return mk<EQ>(lhs ,mk<DIV>(op1, op2));
	case BinaryOperator::SDiv:
	  return trueE;
	  //return mk<EQ>(lhs ,mk<DIV>(op1, op2));
	  // XXX need to check for non linear division/multiplication
        }

      assert (0 && "Unreachable");
    }

    /* Over approximates a comparisson instruction */
    Expr doCompare (Expr lhs, const ICmpInst &icmp)
    {

      const Value* v1 = icmp.getOperand(0);
      const Value* v2 = icmp.getOperand(1);


      Expr op1 = lookup (v1);
      Expr op2 = lookup (v2);
        
      int pred = icmp.getSignedPredicate();

      switch(pred)
	{
	case CmpInst::ICMP_EQ:
	  return mk<IFF>(lhs, mk<EQ>(op1,op2));
	case CmpInst::ICMP_NE:
	  return mk<IFF>(lhs, mk<NEQ>(op1,op2));
	case CmpInst::ICMP_UGT:
	case CmpInst::ICMP_SGT:
	  return mk<IFF>(lhs,mk<GT>(op1,op2));
	case CmpInst::ICMP_UGE:
	case CmpInst::ICMP_SGE:
	  return mk<IFF>(lhs,mk<GEQ>(op1,op2));
	case CmpInst::ICMP_ULT:
	case CmpInst::ICMP_SLT:
	  return mk<IFF>(lhs,mk<LT>(op1,op2));
	case CmpInst::ICMP_ULE:
	case CmpInst::ICMP_SLE:
	  return mk<IFF>(lhs,mk<LEQ>(op1,op2));
	}

      assert (0 && "Unreachable");
    }
    
    Expr doStatement(const Instruction* i, const BasicBlock* bb) 
    {
      //errs () << "\nDoing Statement "<<*i <<"\n";
      int opc = i->getOpcode();
       
      // -- instructions that do not produce a value
      if (opc == BranchInst::Br || opc == ReturnInst::Ret)
	return trueE;

      // -- create new non-deterministic value
      Expr lhs = env.refresh (*i);

      // -- call statement (over-approximate by nondet)
      if (opc == CallInst::Call)
        return trueE;

      // -- floating point compare (over-approximate by nondet)
      if (opc == FCmpInst::FCmp)
	return trueE;

      // -- if not tracking pointers, and i is not an integer type,
      // -- replace by non-determinism
      if (!trackPtr && !i->getType ()->isIntegerTy ()) return trueE;

      // -- handle ZExt / SExt
      if (opc == ZExtInst::ZExt || opc == SExtInst::SExt)
	{ 
	  const Value* rhs = i->getOperand(0);
	  Expr op1;
        
	  if (isa<Constant>(rhs))
	    {
	      const ConstantInt* ci;
	      if ((ci = dyn_cast<ConstantInt>(rhs)) != NULL)
		{
		  // -- promote boolean integer
		  if (isBoolType (ci->getType ()))
		    op1 = mkTerm (ci->isOne () ? 1 : 0, efac);
		  else // -- a noop
		    {
		      mpq_class k = toMpz (ci->getValue ());
		      return mkTerm (k, efac);
		    }
		}
	      else
		assert(isa<ConstantInt>(rhs) && 
		       "ZExt/SExt over non-integer types are unsupported");
	    }
	  // Boolean variable (promote to int)
	  else if (isBoolType (rhs->getType ()))
	    op1 = mk<ITE>(env.lookup(*rhs), mkTerm(1,efac), mkTerm(0,efac));
	  else // int variable 
	    op1 = env.lookup (*rhs);
	
	  return mk<EQ>(lhs, op1);
	}

      if (opc == TruncInst::Trunc)
	{
	  const Value* rhs = i->getOperand(0);
	  return mk<EQ> (lhs, env.lookup (*rhs));
	}
      
      // -- treat all other casts as noops
      if (isa<CastInst> (i)) 
	return mk<EQ> (lhs, env.lookup (*(i->getOperand (0))));

      

      //assert (opc != TruncInst::Trunc);

      // -- handle select statement
      if (opc == SelectInst::Select)
	{
	  const SelectInst *si = cast<SelectInst>(i);
        
	  const Value *c = si->getCondition();
	  const Value *v1 = si->getTrueValue();
	  const Value *v2 = si->getFalseValue();
        

	  //errs () << "SELECT: " << c << " " << v1 << " " << v2 << "\n" ;
        
	  Expr cond;
	  Expr op1;
	  Expr op2;
        
	  const ConstantInt* ci;
	  if ((ci = dyn_cast<ConstantInt>(c)) != NULL)
	    cond = ci->isOne () ? trueE : falseE;	
	  else cond = env.lookup(*c);

	  op1 = lookup (v1);
	  op2 = lookup (v2);
        
	  return mk<EQ>(lhs, mk<ITE>(cond , op1, op2));
	}

      switch(opc)
	{	
        case BinaryOperator::And:
	case BinaryOperator::Or:
	case BinaryOperator::Xor:
	case BinaryOperator::Shl:
	case BinaryOperator::AShr:
	case BinaryOperator::LShr:
	  return doLogic(lhs, *i);
        
        case ICmpInst::ICmp:
          return doCompare(lhs, cast<ICmpInst>(*i));


	case BinaryOperator::Add:
	case BinaryOperator::Sub:
	case BinaryOperator::Mul:
	case BinaryOperator::UDiv:
	case BinaryOperator::SDiv:
	  return doArith(lhs, *i);
	
        default:
	  // -- do nothing -- non-deterministic assignment
	  //DEBUG(errs () << "default: " <<  *i << "\n");
	  return trueE;
	}
    }
     
    Expr doLastAction()
    {
      const BasicBlock* dstBB = dst.getBB();
      
      Expr actions = trueE;
      
      // -- do statements iff it's the return BB
      if (dstBB->getTerminator()->getOpcode() == ReturnInst::Ret)
	{
        
	  for (BasicBlock::const_iterator it = dstBB->begin(), 
		 end = dstBB->end (); it != end; ++it) 
	    {
	      // -- skip phi node
	      if (isa<PHINode> (*it)) continue;
	  
	      actions = boolop::land(actions,
				     doStatement(&*it, dstBB));
	    }
      
	}

      return actions;
    }

    // -- returns the condition of the edge
    // -- without called functions
    Expr getCond() const { assert (computed); return cond; }

    
    const SEdgeCondComp& compute ()
    {
      assert (!computed);
      
      std::vector<Expr> actions;
      std::vector<Expr> control;
      
      // -- all blocks are dealt with similarly
      Expr r;
        
      // -- for every BB on an sedge
      for (SEdge::const_iterator bb = edge->begin(),
	     bbend = edge->end (); bb != bbend; ++bb)
	{
	  r = doControl (*bb);
	  if (r != trueE) control.push_back (mkAssume (r));
	    
        	
	  // -- for every statement of a bb
	  for (BasicBlock::const_iterator i = (*bb)->begin(), 
		 e = (*bb)->end(); i != e; ++i)
	    {
	      // -- except for PHINodes
	      if (isa<PHINode> (*i)) continue;

	      r = doStatement (i, *bb);
	      if (r != trueE) actions.push_back (mkAssume (r));
	    }
	}
      
      r = doLastControl ();
      if (r != trueE) /* XXX no assumptions. Assumptions here seem to
			 slow down the analysis a lot*/
	control.push_back(r);
      
      //This looks at the destination cutpoint
      //and checks to see if the last cutpoint
      //in a function, i.e. ends with a return statement.
      //If so, then it returns an expression for its actions, else
      //returns trueE.
      r = doLastAction ();
      if (r != trueE) actions.push_back (mkAssume (r));

      if (actions.empty ()) actions.push_back (trueE);
      if (control.empty ()) control.push_back (trueE);
      
      Expr c = boolop::land (control.begin(), control.end());
      Expr a = boolop::land (actions.begin(), actions.end());
      cond = boolop::land (c, a);
      computed = true;
      return *this;
    }
    
    /** Computes DAG Condition of a given edge. Environment is updated
	to the end of the edge 
    */
    static Expr computeEdgeCond (ExprFactory &efac, 
				 Environment &env, 
				 SEdgePtr edg, 
				 LBE &lbe)
    {
      Expr cond = SEdgeCondComp (efac, env, edg, lbe).compute ().getCond ();
      updateEnvToFirstInst (env, *edg->getDst ());
      return cond;
    }

    /**
     * Computed DAG condition of a given edge starting with a pre-condition.
     * Environment is updated to be at the end of the edge
     */
    static Expr computeEdgeCond (ExprFactory &efac, 
				 Environment &env,
				 Expr pre,
				 SEdgePtr edg,
				 LBE &lbe)
    {
      updateEnvToFirstInst (env, *edg->getSrc ());
      Expr nPre = env.eval (pre);
      Expr eCond = computeEdgeCond (efac, env, edg, lbe);
      return boolop::land (nPre, eCond);
    }
    
    
    
  };


  
  namespace detail
  {
    VisitAction EvalVisitor::operator () (Expr exp) const
    {
      // -- if bound, return value
      if (env.isBound (exp))
	return VisitAction::changeTo (env.lookup (exp));

      // -- if unbound, but is a terminal that environment knows about
      // -- implicitly
      if (isOpX<BB> (exp) || isOpX<VALUE> (exp) || 
	  isOpX<NODE> (exp) || isOpX<VARIANT> (exp)){
	return VisitAction::changeTo (env.lookup (exp));
      }
      return VisitAction::doKids ();
    }
  }

  /** returns a path from entry to exit, but not including exit */
  template <typename Smt, typename OutputIterator>
  void doCfgCex (const BasicBlock *en, const BasicBlock *ex, 
		 Environment &edgEnv,
		 Smt &smt, OutputIterator out)
  {
    const BasicBlock *v;
    v = en;
    *(out++) = en;
    
    do
      {
	const BasicBlock* newV = NULL;
	bool modelEdge = false;
	
	assert (edgEnv.isBound (*v));
	Expr src = edgEnv.lookup (*v);

	// for all successors
	for (succ_const_iterator it = succ_begin (v), end = succ_end (v); 
	     it != end; ++it)
	  {
	    const BasicBlock *u = *it;

	    // -- the edge variable to the exit variable is missing
	    // -- so if u is exit node, we guess that edge, unless some 
	    // -- other edge variable will reset it
	    if (u == ex) 
              {
                newV = u;
                continue;
              }

	  
	    // -- skip all successors that are not on the right cpg-edge
	    if (!edgEnv.isBound (*u)) continue;

	    if (!newV) newV = u;

	    Expr dst = edgEnv.lookup (*u);
	    Expr edgTerm = detail::mkEdgVar (src, dst);
	    if (isOpX<TRUE> (smt.getModelValue (edgTerm)))
	      {
		modelEdge = true;
                newV = u;
		break;
	      }
	  }
	assert (newV && "Could not guess a counterexample!!!");
	
	if (newV != ex) *(out++) = newV;
	if (!modelEdge && newV != ex)
	  errs () << "WARNING: NO MODEL: guessed cex step: " 
		  << v->getName () << " --> "
		  << newV->getName () << "\n";
	v = newV;
      } 
    while (v != ex);
  }
}

#endif
