#ifndef __ARG_H_
#define __ARG_H_

#include "ufo/ufo.hpp"
#include "ufo/Cpg/Lbe.hpp"
#include <list>

#include "ufo/Asd/AbstractStateDomain.hpp"

using namespace ufo;
namespace ufo 
{

  typedef unsigned int id_ty;

  class Node;  
  typedef std::pair<Node*,Node*> NodePair;
  typedef std::vector<Node*> NodeVector;
  typedef std::set<Node*> NodeSet;
  typedef std::list<Node*> NodeList;

  
    

  /** A node of an ARG */
  class Node
  {
  protected:
    // -- unique identifier
    id_ty id;

    /** Corresponding program location */
    const CutPointPtr loc;
       
    // -- the label
    Expr label;

    // -- abstract state
    AbstractState astate;


    // -- List of parents
    NodeVector parents;

    // -- List of chidlren. 
    NodeVector children;
      
    /** list of nodes that cover this node */
    NodeVector coveredBy;

    /** a boolean mark */
    bool marked;

    /**  last iteration that visited this node */
    unsigned visit; 

  private:
    Node (const Node &other) {}

  public:
    typedef NodeVector::const_iterator const_iterator;
    
    // -- root node constructor
    Node (id_ty nid, const CutPointPtr ncp):
      id(nid), loc(ncp),  marked(false), visit(0) {}

    const_iterator begin () const
    {
      return children.begin ();
    }
    const_iterator end () const
    {
      return children.end ();
    }
    
    const_iterator pbegin () const 
    {
      return parents.begin ();
    }
    const_iterator pend () const
    {
      return parents.end ();
    }
    
    size_t inDegree () const
    {
      return parents.size ();
    }
    size_t outDegree () const 
    {
      return children.size ();
    }
    
    
    // -- adds a new child
    void addChild (Node  &child) 
    { 
      children.push_back (&child); 
      child.parents.push_back (this);
      //      child.addParent (this);
    }

    void removeChild (Node* child)
    {
      bool removed = false;
      for(std::vector<Node*>::iterator i = children.begin(), 
	    e = children.end(); i!=e; ++i){
        if (*i == child){
	  children.erase(i);
	  removed = true;
	  break;
	}
      }

      assert(removed);
      removed = false;
      for(std::vector<Node*>::iterator i = child->parents.begin(), 
	    e = child->parents.end(); i!=e; ++i){
        if (*i == this){
	  child->parents.erase(i);
	  removed = true;
	  break;
	}
      }

      assert(removed);
    }

    const CutPointPtr getLoc () const { return loc; }
    
    bool isMarked () const { return marked; }

    void mark () { marked = true; }
    void unmark () { marked = false; }
    
    unsigned getVisit () const { return visit; }
    void setVisit (unsigned v) { visit = v; }
    
    bool isCovered () const { return coveredBy.size () > 0; }
    
    template <typename InputIterator>
    void coverBy (InputIterator begin, InputIterator end)
    {
      if (begin == end) return;
      coveredBy = std::vector<Node*> (begin, end);
    }
    
    void uncover () { coveredBy.clear (); }
    
    const std::vector<Node*> &getParents () const { return parents; }
    const std::vector<Node*> &getChildren () const { return children; }
    const std::vector<Node*> &getCoveredBy () const { return coveredBy; }

    Expr getLabel () const { return label; }
    AbstractState getState () const { return astate; }

    /** Sets the label. Resets the abstract state */
    void setLabel (Expr v) { label = v; astate.reset (); }
    /** Sets the abstract state. Resets the label */
    void setState (const AbstractState v) { astate = v; label.reset (); }

    id_ty getId () const { return id; }

    std::string getNameStr ()
    {
      return boost::lexical_cast<string> (getId ()) + ":" + 
	getLoc ()->getBB ()->getNameStr ();
    }
    
    
  };  
  
  class ARG
  {
  private:
    // -- a unique identifier 
    id_ty id;

    /** Function of the ARG */
    const Function *func;

      
    /** List of all the nodes */
    NodeVector nodes;
    std::set<NodePair> falseEdges;
    

    // // -- initial and error cutpoints
    // id_ty li;
    // id_ty le;
    
  public:
    ARG (const ARG &other) : 
      id(other.id), func(other.func), nodes(other.nodes) {}
    ARG(id_ty i, const Function &f) : id(i), func(&f) {}

    ~ARG () { foreach (Node* n, nodes) delete n; }
    
    typedef NodeVector::const_iterator const_iterator;
    const_iterator begin () const { return nodes.begin (); }
    const_iterator end () const { return nodes.end (); }

    Node* newNode (CutPointPtr cp)
    {
      nodes.push_back (new Node (nodes.size (), cp));
      return nodes.back ();
    }

    id_ty getId() const { return id; }
    const Function* getFunction () const {return func; }
    Node* nodeById (id_ty id) const { return nodes [id]; }
    Node* getRoot () const { return nodeById (0); }
    size_t size () const { return nodes.size (); }
    const std::vector<Node*> &getNodes () { return nodes; }
    void setFalseEdge (Node* u, Node* v) 
    {
      falseEdges.insert (NodePair (u, v));
    }
    void clearFalseEdge (Node *u, Node *v)
    {
      falseEdges.erase (NodePair (u, v));
    }
    bool isFalseEdge (Node *u, Node *v) const
    {
      return falseEdges.count (NodePair (u, v)) > 0;
    }

    /** 
     * Returns a null vertex. 
     *
     * This appears to be needed by the BGL. It mirrors the function
     * graph_traits<ARG>::null_vertex(), so it is not clear why this
     * is needed. Perhaps a bug in BGL, but somehow this only caused a
     * problem on an older compiler (gcc (Ubuntu/Linaro 4.4.4-14ubuntu5.1) 4.4.5)
     * 
     * \return NULL
     **/
    static Node* null_vertex () { return NULL; }
  };
}

namespace std
{
  /** standard order of nodes by id */
  template <> struct less<ufo::Node*>
  {
    bool operator() (const ufo::Node *x, const ufo::Node *y) const
    {
      if (x == NULL) return y != NULL;
      if (y == NULL) return false;
      
      return x->getId () < y->getId ();
    }
    
  };
}

namespace expr
{

  template<> struct TerminalTrait<ufo::Node*>
  {
    static inline void print (std::ostream &OS, ufo::Node* n, 
			      int depth, bool brkt) ;
    static inline bool less (ufo::Node* s1, ufo::Node* s2);
    static inline bool equal_to (const ufo::Node *n1, const ufo::Node *n2);
    static inline size_t hash (const ufo::Node *n);
  };


  void TerminalTrait<ufo::Node*>::print (std::ostream &OS, 
					  ufo::Node* n, 
					  int depth, bool brkt) 
  {   
    OS << "NID(" << n->getId () << ")";
  }

  bool TerminalTrait<ufo::Node*>::less (ufo::Node* s1, 
					 ufo::Node* s2)
  { return s1->getId() < s2->getId(); }

  bool TerminalTrait<ufo::Node*>::equal_to (const ufo::Node* s1, 
					     const ufo::Node* s2)
  {
    return s1 == s2;
  }
  
  size_t TerminalTrait<ufo::Node*>::hash (const ufo::Node *n)
  {
    boost::hash<const ufo::Node*> hasher;
    return hasher (n);
  }

  typedef expr::Terminal<ufo::Node*> NODE;  

  
}



#endif
