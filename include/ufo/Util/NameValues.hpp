#ifndef __NAME_VALUES__HPP_
#define __NAME_VALUES__HPP_

#include "llvm/Pass.h"
#include "llvm/Module.h"
#include "llvm/Function.h"




namespace llvm
{
  struct NameValues : public ModulePass
  {
    static char ID;
    NameValues () : ModulePass (ID) {}
    bool runOnModule (Module &M);
    bool runOnFunction (Function &F);
    void getAnalysisUsage (AnalysisUsage &AU) const { AU.setPreservesAll (); }
  };
}

#endif
